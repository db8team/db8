import { combineReducers } from 'redux'
import users from './users.reducer'
import nav from './nav.reducer'
import questions from './questions.reducer'
import debate from './debate.reducer'
import {GET_QUESTIONS_REQUESTED, GET_QUESTIONS_SUCCESS,GET_QUESTIONS_FAILED} from '../actions/questions.actions'



export default combineReducers({
  user: users,
  questions,
  nav,
  debates: debate
})