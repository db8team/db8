
import {
  GET_QUESTIONS_REQUEST, 
  GET_QUESTIONS_SUCCESS,
  GET_QUESTIONS_FAILED,
  GET_TIME_TILL_LIVE_REQUESTED,
  GET_TIME_TILL_LIVE_SUCCESS,
  GET_TIME_TILL_LIVE_FAILED
} from '../actions/questions.actions'

const initState = {
  live: [],
  practice: [],
  all: [],
  loadedAt: null,
  liveTime: null,
  requestingNow: false,
  pairingTill: null,
  ms: null
}

const questions = (state = initState, action) => {
  switch (action.type) {
    case GET_QUESTIONS_SUCCESS :
      return {
        ...state,
        loadedAt: new Date(),
        live: action.live,
        all: action.all,
        requestingNow: false,
        practice: action.practice
      }
    case GET_QUESTIONS_REQUEST :
      return{
        ...state,
        requestingNow: true
      }
    case GET_QUESTIONS_FAILED :
      return{
        ...state,
        requestingNow: false
      }
    case GET_TIME_TILL_LIVE_SUCCESS:
      return {
        ...state,
        liveTime: action.liveTime,
        pairingTill: action.pairingTill,
        ms: action.ms
      }
    default:
      return state
  }
}

export default questions