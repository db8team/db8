
import {
  SIGNIN_SUCCESS,
  SIGNOUT_SUCCESS, 
  SIGNIN_REQUESTED,
  SIGNIN_FAILED
} from 'DB8/actions/users.actions'
import {
  GET_QUESTIONS_REQUESTED, 
  GET_QUESTIONS_SUCCESS,
  GET_QUESTIONS_FAILED,
  GET_TIME_TILL_LIVE_REQUESTED,
  GET_TIME_TILL_LIVE_SUCCESS,
  GET_TIME_TILL_LIVE_FAILED
} from 'DB8/actions/questions.actions'

import {
  PAIR_REQUESTED, 
  PAIR_FAILED,
  PAIR_SUCCESS,
  PAIR_WAITING,
  END_DEBATE_REQUESTED,
  END_DEBATE_FAILED, 
  END_DEBATE_SUCCESS, 
  SELECT_DEBATE,
  GET_DEBATES_REQUESTED,
  GET_DEBATES_SUCCESS,
  GET_DEBATES_FAILED,
  LISTENER_FAILED
} from 'DB8/actions/debate.actions'

import {
  CHANGE_VIEW,
  CHANGE_NAV_COLOR,
  GO_BACK,
  ALERT,
} from 'DB8/actions/nav.actions'

const pageDefaults = {
  landing: {
    currentView:'Landing',
    bottomNav: false,
    props:{},
    navType: 'dark',
    loading: false
  },

  questions: {
    currentView: 'Questions', 
    bottomNav: true,
    nestedBottomNav: true,
    props:{},
    navType: 'dark',
    loading: false
  },
  debate: {
    currentView: 'Debate',
    bottomNav: false,
    props:{},
    navType: 'dark',
    loading: false
  },
  liveQuestions: {
    currentView: 'QuestionsList',
    bottomNav: true,
    props:{type: "live", navComponent: true},
    navType: 'light',
    loading: false
  },
  practiceDebatesList: {
    currentView: 'PracticeDebatesList',
    bottomNav: true,
    props:{},
    navType: 'dark',
    loading: false
  },
  profile:{
    currentView: 'Profile',
    bottomNav: true,
    props:{},
    navType: 'dark',
    loading: false
  },
  settings: {
    currentView: 'Settings',
    bottomNav: true,
    props:{},
    navType: 'dark',
    loading: false
  }
}

const initNav = {
  history: [],
  token: null,
  alertTitle: null,
  alertText: null,
  ...pageDefaults.landing
}


const addToHistoryActions = [
  CHANGE_VIEW,
  SELECT_DEBATE,
]

const nullAlert = {
  alertTitle: null,
  alertText: null,
}
const nav = (state = initNav, action) => {

  let {history, ...copiedState} = state

  let updatedHistory = updateHistory(copiedState,action,history)
  let navState = getPageChange(state,action) || state
  let loading = getLoadingChange(navState.loading,action) 
  let alertObj = getAlertObj(action)
  console.log('action: ',action.type)
  return {...navState, ...alertObj, loading, history: updatedHistory}
}

const updateHistory = (state,action,currentHistory) => {
  const addActions = [
    CHANGE_VIEW,
    SELECT_DEBATE
  ]
  let prevHistory = [...currentHistory]
  prevHistory.splice(0,1)

  switch (action.type) {
    case CHANGE_VIEW :
      return [{...state}, ...currentHistory]
    case SELECT_DEBATE : 
      return [{...state, props: {previousSlected: action.debate.id}}, ...currentHistory]
    case GO_BACK :
      return prevHistory
    default :
      return currentHistory
  }

}

const composeAlert = (action) => {
  // console.log('test',typeof action.error[0])

  if (action.error.message && typeof action.error.message === "string") {
    if (action.error.error && action.error.error.message) {
      if (action.error.error.message === 'Network request failed') {
        return {
          alertTitle: "Network Error",
          alertText: "Please connect to the internet."
        }
      }else{
        
      }
    }
    return{
      alertTitle: "Error",
      alertText: action.error.message
    }
  }else{
    return {
      alertTitle: "Error",
      alertText: "Ooops. Something went wrong."
    }
  }
}
const getAlertObj = (action) => {
  switch(action.type) {
    case GET_QUESTIONS_FAILED :
    case GET_TIME_TILL_LIVE_FAILED :
    case GET_DEBATES_FAILED:
    case LISTENER_FAILED:
    case PAIR_FAILED:
    case ALERT:
      return composeAlert(action)
    case SIGNIN_FAILED:
      console.log(action)
      if (action.error.error && action.error.error.type === 'cancel') {
        return nullAlert
      }else{
        return composeAlert(action)
      }
    default: 
      return nullAlert
  }
}

const getLoadingChange = (loading,action) => {
  switch (action.type) {
    case GET_QUESTIONS_REQUESTED :
    case PAIR_REQUESTED :
    case GET_TIME_TILL_LIVE_REQUESTED: 
    case GET_DEBATES_REQUESTED:
    case SIGNIN_REQUESTED:
      return true
    case GET_QUESTIONS_SUCCESS :
    case GET_TIME_TILL_LIVE_SUCCESS :
    case GET_DEBATES_SUCCESS:
    case SIGNIN_FAILED:
      return false
    default:
      return loading
  }
}
const getPageChange = (state = initNav, action) => {
  switch (action.type) {
    case SIGNIN_SUCCESS:
      return {
        ...state,
        ...pageDefaults.questions,
        token:action.payload.token
      }

    case PAIR_FAILED :
    case END_DEBATE_FAILED:
    case END_DEBATE_SUCCESS:
      return {
        ...state,
        ...pageDefaults.questions,
      }

    case SELECT_DEBATE:
    case PAIR_WAITING:
    case PAIR_SUCCESS:
      return {
        ...state,
        ...pageDefaults.debate,
      }

    case SIGNOUT_SUCCESS:
      return initNav

    case CHANGE_VIEW:
      let newState ={
        ...state,
        ...pageDefaults[action.view],
      }
      if (action.props){
        newState.props = action.props
      }
      return newState

    case CHANGE_NAV_COLOR:
      return {
        ...state,
        navType: action.navType
      }

    case GO_BACK: 
      let oldState = {...state.history[0]}
      return {
        ...oldState,

      }
    default:
      return null
  }
}

export default nav