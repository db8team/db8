import {
  PAIR_REQUESTED, 
  PAIR_FAILED, 
  PAIR_SUCCESS,
  PAIR_WAITING, 
  END_DEBATE_FAILED, 
  END_DEBATE_SUCCESS, 
  GET_DEBATES_SUCCESS,
  UPDATE_DEBATE,
  LISTENER_DISPATCHED,
  LISTENER_STOPED,
  SELECT_DEBATE,
  SEND_MESSAGES_SUCCESS
} from '../actions/debate.actions'

const initState = {
  activeDebate: null,
  debates: null
}

const debate = (state = initState, action) => {
  switch (action.type) {
    case SELECT_DEBATE :
    case PAIR_WAITING :
    case PAIR_SUCCESS :
      return {
        ...state,
        activeDebate: {
          ...action.debate,
          answer: action.answer,
          unsubscribe: null,
        }
      }
    case GET_DEBATES_SUCCESS:
      if (action.update) {
        return{
          ...state,
          activeDebate: null,
          debates: action.debates,
        }
      }
    case UPDATE_DEBATE :
      return {
        ...state,
        activeDebate : {
          ...state.activeDebate,
          ...action.payload
        }
      }
    case LISTENER_DISPATCHED: 
      return {
        ...state,
        activeDebate : {
          ...state.activeDebate,
          unsubscribe: action.payload
        }
      }
    case END_DEBATE_FAILED:
    case END_DEBATE_SUCCESS:
      return {
        ...state,
        activeDebate: null
      }
    default:
      return {
        ...state,
      }
  }
}

export default debate