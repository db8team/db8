import {SIGNIN_SUCCESS, SIGNOUT} from '../actions/users.actions'
import {AsyncStorage} from 'react-native';


const initUser = {
  UID: '',
  name: ''
}
const users = (state = initUser, action) => {

  switch (action.type) {
    case SIGNIN_SUCCESS:
      if (action.jwtInit) {
        console.log(action.payload.token)
        AsyncStorage.setItem('jwtToken',action.payload.token)
      }
      return action.payload
    case SIGNOUT:
      return initUser
    default:
      return state
  }

}

export default users