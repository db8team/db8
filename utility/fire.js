import * as firebase from 'firebase';
import 'firebase/firestore';
// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyB3LJWNltpWywAm6noqqMYK7-B2DVEngjI",
  authDomain: "de-b8-69cd3.firebaseapp.com",
  databaseURL: "https://de-b8-69cd3.firebaseio.com",
  projectId: "de-b8-69cd3",
  storageBucket: "de-b8-69cd3.appspot.com"
};

firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

db.settings({
  timestampsInSnapshots: true
});

const loginWithGoogle = async () => {
  try {
    const result = await Expo.Google.logInAsync({

      androidClientId: "928464225835-jr9bk1jok01i9ar5vbnk21o8q5esk4rb.apps.googleusercontent.com",
      iosClientId: "928464225835-14qmstf21eavmdr34f2efvstohr4kkqe.apps.googleusercontent.com",
      scopes: ['profile', 'email'],
      behavior: "web"
    });

    if (result.type === 'success') {
      firebaseAuth(firebase.auth.GoogleAuthProvider.credential(result.idToken));
    } else {
      console.log(result)
      return {cancelled: true};
    }
  } catch(e) {
    return {error: true};
  }
}

const loginWithFacebook = async () => {
  const result = await Expo.Facebook.logInWithReadPermissionsAsync(
    '514259105707439',
    { permissions: ['public_profile'] }
  );

  if (result.type === 'success') {
    // Build Firebase credential with the Facebook access token.
    const credential = firebase.auth.FacebookAuthProvider.credential(token);

    // Sign in with credential from the Facebook user.
    firebase.auth().signInAndRetrieveDataWithCredential(credential).catch((error) => {
      // Handle Errors here.
      console.log(error);
    });
  }else{
    console.log(result)
  }
}


module.exports = {
  loginWithFacebook,
  loginWithGoogle,
  firebase,
  db
}