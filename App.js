import React from 'react';
import {SafeAreaView} from 'react-native';
import { authInitListener} from 'DB8/utility/fire'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware} from 'redux'
import configureStore from 'DB8/utility/configureStore'

import Nav from 'DB8/components/nav/nav'
console.disableYellowBox = true
const store = configureStore()
export default class App extends React.Component {
  state = {
    ready: false,
  }
  render() {
    return (
      <Provider store={store}>
        <Nav />
      </Provider>
      
    );
  }
}
