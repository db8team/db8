import React, { Component } from 'react';
import { 
  Alert, 
  Text, 
  View,
  StyleSheet,
  Dimensions,
  Animated
} from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';
import Colors from 'DB8/assets/colors';
import {Feather} from '@expo/vector-icons';
import {
  signOut
} from 'DB8/actions/users.actions'
import {
  changeView,
} from 'DB8/actions/nav.actions'
import { ifIphoneX } from 'react-native-iphone-x-helper'

const {height, width} = Dimensions.get('window');
const selectedViewsByBtn = {
  NewsFeed : ['NewsFeed'],
  Profile: ['Profile','Settings'],
  Questions: ['QuestionsList','Questions'],
  Debates: ['Debate', 'PracticeDebatesList'],
  Discover: ['Discover']
}
class BottomNavBar extends Component {
  state ={
    btnLayout: ['NewsFeed','Discover','Questions','Debates','Profile'],
    leftValue: new Animated.Value(2*width/5)
  }
  componentDidMount() {
    this.animateSelect()
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.nav.currentView != this.props.nav.currentView) {
      this.animateSelect()
    }
  }


  onBtnPress = () =>{
    Alert.alert(
      `Coming Soon`,
      `In Development!`,

    )
  }
  animateSelect(){
    for(let i in this.state.btnLayout) {
      if (this.isSelected(this.state.btnLayout[i])) {
        Animated.spring(                  // Animate over time
          this.state.leftValue,            // The animated value to drive
          {
            toValue: i*width/5,                   // Animate to opacity: 1 (opaque)
            duration: 300,              // Make it take a while
          }
        ).start()
        break
      }
    }

    return 'not found'
  }
  calcIconStyle(btnName){
    let style = []
    if (this.props.type === 'light') {
      style.push(styles.lightIcon)
      if (this.isSelected(btnName)) {
        style.push(styles.selectedlightIcon)
      }
    }else{
      style.push(styles.icon)
      if (this.isSelected(btnName)) {
        style.push(styles.selectedIcon)
      }
    }

    
    return style
  }
  isSelected(btnName){

    let selectedViews = selectedViewsByBtn[btnName]
    return selectedViews && selectedViews.includes(this.props.nav.currentView)
  }
  
  render() {
    let currentView = this.props.nav.currentView
    return (
      <View style={this.props.overlay? [styles.container, styles.overlayContianer] : styles.container}>
        <View style={styles.indicatorContainer}>
          <Animated.View style={{...styles.indicator, left: this.state.leftValue, backgroundColor: this.props.type === 'light' ? 'white' : Colors.defaultBlue}}>
          </Animated.View>
        </View>
        <View style={styles.navBar}>  
          <Button
            onPress={this.onBtnPress}
            buttonStyle={this.isSelected('NewsFeed') ? (this.props.type === 'light'? [styles.selectedBtn, styles.selectedLightBtn] : styles.selectedBtn) : styles.btn}
            icon={
              <Feather style={this.calcIconStyle('NewsFeed')} name="home" size={25}  />
            }
            type="clear"
          />
          <Button
            onPress={this.onBtnPress}
            buttonStyle={this.isSelected('Discover') ? (this.props.type === 'light'? [styles.selectedBtn, styles.selectedLightBtn] : styles.selectedBtn) : styles.btn}
            icon={
              <Feather style={this.calcIconStyle('Discover')} name="search" size={25}  />
            }
            type="clear"
          />
          
          <Button
            onPress={() => this.props.changeView('questions')}
            buttonStyle={this.isSelected('Questions')? (this.props.type === 'light'? [styles.selectedBtn, styles.selectedLightBtn] : styles.selectedBtn) : styles.btn}
            icon={
              <Feather style={this.calcIconStyle('Questions')} name="mic" size={25}  />
            }
            type="clear"
          />
          <Button
            onPress={() => this.props.changeView('practiceDebatesList')}
            buttonStyle={this.isSelected('Debates')? (this.props.type === 'light'? [styles.selectedBtn, styles.selectedLightBtn] : styles.selectedBtn) : styles.btn}
            icon={
              <Feather style={this.calcIconStyle('Debates')} name="list" size={25}  />
            }
            type="clear"
          />
          <Button
            onPress={() => this.props.changeView('profile')}
            buttonStyle={this.isSelected('Profile') ? (this.props.type === 'light'? [styles.selectedBtn, styles.selectedLightBtn] : styles.selectedBtn) : styles.btn}
            icon={
              <Feather style={this.calcIconStyle('Profile')} name="user" size={25}  />
            }
            type="clear"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    height: 50,
    width,
    borderColor: Colors.darkerGrey,

  },
  indicatorContainer:{
    width,
    height: 1,

  },
  indicator:{
    height:2,
    borderRadius: 1,
    width:width/5,
    position: 'relative',
    top: 0,
  },
  navBar:{
    
    flex:1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    
  },
  overlayContianer:{
    borderColor: 'transparent',
    backgroundColor: 'transparent',
    borderWidth: 0,
    borderTopColor: 'transparent',
    position: 'absolute',
     ...ifIphoneX({
      bottom: 10
    }, {
      bottom: 0
    }),

  },
  icon:{
  },
  lightIcon:{
    color: Colors.white
  },
  selectedIcon:{
    fontWeight: 'bold',
  },
  selectedLightIcon:{
    color: 'white'
  },
  btn:{
    paddingLeft: 20,
    paddingRight: 20,
    borderTopWidth: 2,
    borderColor: 'transparent'
  },
  selectedBtn:{
    // bottom: 12,
    // height: 55,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 0,
    borderColor: Colors.darkBlue
  },
  selectedLightBtn: {
    borderColor: Colors.white
  }
})

BottomNavBar.defaultProps = {
  colorScheme: 'dark',
}
const mapStateToProp = ({app,nav, user}) => {
  return {
    nav
  }
}

const mapDispatchToProps = dispatch => {
  return{
    changeView: (view) => dispatch(changeView(view))
  }
}
export default connect(mapStateToProp,mapDispatchToProps)(BottomNavBar)

