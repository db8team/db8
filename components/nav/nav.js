import React from 'react';
import { connect } from 'react-redux'

import { 
  StyleSheet, 
  Text, 
  View,
  Dimensions,
  AsyncStorage,
  AppLoading,
  BackHandler,
  Animated
} from 'react-native';

import {
  authInitListener, 
  signOut,
  signinSuccess,
} from 'DB8/actions/users.actions'

import {
  goBack,
  clearAlert,
} from 'DB8/actions/nav.actions'

import PropTypes from 'prop-types'
import { 
  ifIphoneX, 
  getStatusBarHeight 
} from 'react-native-iphone-x-helper'
import {
  Font,
  SplashScreen,
  Permissions,
  Notifications,
  Constants,
} from 'expo';

import Landing from 'DB8/components/auth/landing'
import Questions from 'DB8/components/questions/questions'
import QuestionsList from 'DB8/components/questions/questionsList'
import Debate from 'DB8/components/debate/debate'
import PracticeDebatesList from 'DB8/components/debate/practiceDebatesList'
import Settings from 'DB8/components/settings/settings'
import Loading from 'DB8/components/common/loading'
import BottomNavBar from 'DB8/components/nav/bottomNavBar'
import Profile from 'DB8/components/profile/profile'
import { Button } from 'react-native-elements'

import Alert from 'DB8/components/elements/alert'
import Config from 'DB8/config/config'
import Colors from 'DB8/assets/colors';
import RF from "react-native-responsive-fontsize"

import {Feather} from '@expo/vector-icons';

const {height, width} = Dimensions.get('window');
const PUSH_ENDPOINT = 'https://your-server.com/users/push-token';
class Nav extends React.Component {

  constructor(props) {
    super(props)
    this.state={
      fontLoaded: false,
      showAlert: false
    }
  }

  routes = {
    Landing,
    Debate,
    Questions,
    QuestionsList,
    PracticeDebatesList,
    Profile,
    Settings
  }

// async registerForPushNotificationsAsync() {
//     const { status: existingStatus } = await Permissions.getAsync(
//       Permissions.NOTIFICATIONS
//     );
//     let finalStatus = existingStatus;

//     // only ask if permissions have not already been determined, because
//     // iOS won't necessarily prompt the user a second time.
//     if (existingStatus !== 'granted') {
//       // Android remote notification permissions are granted during the app
//       // install, so this will only ask on iOS
//       const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
//       finalStatus = status;
//     }

//     // Stop here if the user did not grant permissions
//     if (finalStatus !== 'granted') {
//       return;
//     }

//     // Get the token that uniquely identifies this device
//     let token = await Notifications.getExpoPushTokenAsync();

//     // POST the token to your backend server from where you can retrieve it to send push notifications.
//     return fetch(PUSH_ENDPOINT, {
//       method: 'POST',
//       headers: {
//         Accept: 'application/json',
//         'Content-Type': 'application/json',
//       },
//       body: JSON.stringify({
//         token: {
//           value: token,
//         },
//         user: {
//           username: 'Brent',
//         },
//       }),
//     });
//   }
  checkJWT = async (token,backend) =>{
    try{
      let response = await fetch(`${backend}/api/Users/checkJWT`,{method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          token
        }
      })
      let user = await response.json()
      if (!user.uid) {
        return false
      }    
      return {...user,token}
    }catch(error){
      console.log(error)
      return false
    }
    
  }
  async loginOrSignUp(backend) {
    let jwtToken = await AsyncStorage.getItem('jwtToken');
    if (jwtToken) {
      let user = await this.checkJWT(jwtToken,backend)
      return user
    }else{
    return false
    }
  }
  componentDidMount = async () => {

    let fonts = Font.loadAsync({
      'dosis_medium': require('../../assets/fonts/Dosis-Medium.ttf'),
      'dosis':require('../../assets/fonts/Dosis-Regular.ttf'),
      'dosis_bold':require('../../assets/fonts/Dosis-SemiBold.ttf'),
      'open_sans':require('../../assets/fonts/OpenSans-Regular.ttf'),
      'open_sans_bold':require('../../assets/fonts/OpenSans-SemiBold.ttf'),
      'roboto_bold':require('../../assets/fonts/Roboto-Medium.ttf'),
      'roboto':require('../../assets/fonts/Roboto-Regular.ttf')

    });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    let user = await this.loginOrSignUp(Config.backend)
    
    await fonts

    if (!user) {
      this.props.authInitListener()
    }else{
      this.props.signinSuccess(user,false)
    }
    SplashScreen.hide();
    
    this.setState({ fontLoaded: true });

  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentDidUpdate(prevProps, prevState) {
      if (!prevProps.nav.alertTitle  && this.props.nav.alertTitle) {
        this.showAlert()
      }
  }
  
  handleBackPress = () => {
    this.props.goBack()
    return true
  }

  showAlert = () => {
    this.setState({
      showAlert: true
    })
  }

  clearAlert = () => {
    this.props.clearAlert()
    this.setState({
      showAlert: false
    })
  }
  renderLoading() {
    // console.log('loading: ',this.props.nav.loading)
    return(
      <View style={(this.props.nav.loading || !this.state.fontLoaded) ? styles.loaderEnabled : styles.hide}>
        <Loading />
      </View>
    )
  }


  renderView(props,TagName){
    return(
      <View style={props.nav.loading? styles.hide : styles.navContainer} >
        <TagName {...props.nav.props}/>
      </View>
    )
  }
  
  render() {
    const TagName = this.routes[this.props.nav.currentView]

    return(
      <View style={styles.container} >
        
        {
          this.renderLoading()
        }

        {this.renderView(this.props,TagName)}
        {this.props.nav.bottomNav &&
          <BottomNavBar type={this.props.nav.navType} overlay={true}/>
        }
        { this.state.fontLoaded && this.state.showAlert &&
          <Alert error={true} title={this.props.nav.alertTitle} text={this.props.nav.alertText} onFirstOptionPress={this.clearAlert}/>
        }
      </View>
    )
  }
 }

Nav.propTypes = {
  app: PropTypes.shape({

    errorMessage: PropTypes.string
  }),
  nav: PropTypes.shape({
    currentView: PropTypes.string.isRequired,
    loading: PropTypes.bool.isRequired,
  }),
  user: PropTypes.object.isRequired,
}

const mapStateToProp = ({app,nav, user}) => {
  return {
    app,
    nav,
    user
  }
}

const mapDispatchToProps = dispatch => {
  return{
    authInitListener: () => dispatch(authInitListener()),
    signOut: () => dispatch(signOut()),
    signinSuccess: (user,jwtInit) => dispatch(signinSuccess(user,jwtInit)),
    configInit: (config) => dispatch(configInit(config)),
    goBack: () => dispatch(goBack()),
    clearAlert: () => dispatch(clearAlert()),
  }
}


const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor: 'white'
  },
  hide:{
    flex: 0,
    height: 0,
    width: 0,
    overflow: 'hidden'
  },
  navContainer: {
    flex: 1
  },
  bottomNav: {
    height: 50,
  },
  loaderEnabled:{
    flex: 10000, 
    alignItems: 'center',
    justifyContent: 'center',
  },
  loaderDisabled:{
    height: 0,
    width:0,
  },


});

export default connect(mapStateToProp,mapDispatchToProps)(Nav)