import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Dimensions
} from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper'
const {height, width} = Dimensions.get('window');

export default class BottomNavSpacer extends Component {
  render() {
    return (
      <View  style={{
        ...ifIphoneX({
          height: 60
        }, {
          height: 50, 
        }),
        width,
        ...this.props.style
      }}></View>
    );
  }
}
