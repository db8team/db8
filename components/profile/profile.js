import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Dimensions,
  Text,
  StyleSheet,
  Image
} from 'react-native'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { LinearGradient } from 'expo';
import Colors from 'DB8/assets/colors';
import { 
  ifIphoneX, 
  getStatusBarHeight 
} from 'react-native-iphone-x-helper'
import abbreviate from 'number-abbreviate'
import RF from "react-native-responsive-fontsize"
import {Feather} from '@expo/vector-icons';
import { Button } from 'react-native-elements'
import BottomNavSpacer from 'DB8/components/nav/bottomNavSpacer'
import DebateView from 'DB8/components/debate/debateView'
import Config from 'DB8/config/config'
import Carousel from 'react-native-snap-carousel';
import {
  alert,
  changeView
} from 'DB8/actions/nav.actions'
import textTransform from 'DB8/helpers/textTransform'

const {height, width} = Dimensions.get('window');

class Profile extends Component {

  state = {
    profile: this.props.user,
    sideBarVissible: false,
    debates: [],
    answers: [],
  }

  getDebatesByUser = async () => {
    console.log(`${Config.backend}/api/getDebatesByUser?uid=${this.state.profile.uid}&limit=10&offset=${this.state.debates.length}`)
    try{
      let response = await fetch(`${Config.backend}/api/getDebatesByUser?uid=${this.state.profile.uid}&limit=10&offset=${this.state.debates.length}`,{
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          token: this.props.nav.token
        }
      })

      if (response.status != 400) {
        let result = await response.json()
        let answers = result[0].answers
        this.setState({
          answers
        })

      }else{
        throw "getDebatesByUser Failed"
      }
    }catch(error){
      if (error.message) {
        this.props.alert(error.message)
      }else{
        this.props.alert()
      }
      
    }
  }

  componentDidMount() {
      // ?=console.log(this.state.profile.pic_ur+'?height=130'
      this.getDebatesByUser()

  }


  renderUserProfile(){
    return (
      <LinearGradient
        colors={Colors.blueGradient}
        style={styles.profileInfoContainer}
      >
        <View style={[styles.top]}>
          <View style={styles.picContainer}>
            <Image style={styles.profilePic}
              source={{uri:this.state.profile.pic_url+'?height=300'}}
            />
          </View>
          <Text style={styles.name}>{this.state.profile.display_name}</Text>
          {this.state.profile.uid === this.props.user.uid &&
            <View style={styles.configContainer}>
              <Button 
                type="clear"
                onPress={() => this.props.changeView('settings')}
                icon={
                  <Feather color="white" name="settings" size={RF(3.5)}  />
                }
              />
            </View>
          }
          
        </View>
        <View style={styles.info}>
          <View style={[styles.block]}>
            <Text style={[styles.blockTxt,styles.value]}>
               {textTransform(abbreviate(123000000))}
            </Text>
            <Text style={[styles.blockTxt,styles.key]}>
              FOLLOWERS
            </Text>
          </View>

          <View style={[styles.block]}>
            <Text style={[styles.blockTxt,styles.value]}>
              {textTransform(abbreviate(3257))}
            </Text>
            <Text style={[styles.blockTxt,styles.key]}>
              FOLLOWING
            </Text>
          </View>

          <View style={[styles.block]}>
            <Text style={[styles.blockTxt,styles.value]}>
              {textTransform(abbreviate(10))}
            </Text>
            <Text style={[styles.blockTxt,styles.key]}>
              DB8's WON
            </Text>
          </View>

          <View style={[styles.block]}>
            <Text style={[styles.blockTxt,styles.value]}>
              {textTransform(abbreviate(113400))}
            </Text>
            <Text style={[styles.blockTxt,styles.key]}>
              POINTS
            </Text>
          </View>

        </View>
      </LinearGradient>
    )
    
  }
  renderDebateView({item, index}) {
    let debate = item.Debate
    let answers = {}
    debate.answers.forEach((a) => {
      answers[a.uid] = a.answer
    })

    return(
      <DebateView answers={answers} users={debate.Users} messages={JSON.parse(debate.messages)} question={JSON.parse(debate.question)} />  
    )
  }
  renderUserDebates(){
    // answers[0].Debate
    if (this.state.answers.length) {
      return (
        <View style={styles.userDebatesContainer}>
          <Carousel
              ref={(c) => { this._carousel = c; }}
              data={this.state.answers}
              renderItem={this.renderDebateView}
              sliderWidth={width}
              itemWidth={8.5*width/10}
            />
        </View>
      )
    }else{
      <View style={styles.userDebatesContainer} />
    }
  }
  render() {      
    return (
      <View style={styles.container}>
        <View style={styles.container}>
          {this.renderUserProfile()}
          {this.renderUserDebates()}

        </View>
        <BottomNavSpacer style={{backgroundColor: Colors.grey}}/>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container:{
    flex: 1
  },
  nameContainer: {
    flexDirection: 'row',
    ...ifIphoneX({
      marginTop: 10,
    })
  },
  top: {
    flexDirection: 'row',
    padding: 10,
    paddingBottom: 0,
  },
  info: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  configContainer:{
    height: verticalScale(50),
    position: 'absolute',
    right: 10,
    top: 10, 
    justifyContent: 'center',
    alignItems: 'center'
  },
  profileInfoContainer: {
    paddingTop: getStatusBarHeight(),

  },
  userDebatesContainer:{
    flex: 13,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: Colors.blackish,
  },
  row: {
    flex: 1,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  profilePic:{
    height: verticalScale(50), 
    borderRadius: verticalScale(50)/2, 
    width: verticalScale(50),
    borderWidth: 2,
    borderColor: 'white'
  },
  name: {
    color: 'white',
    fontFamily: 'dosis_bold',
    fontSize:27,
    lineHeight: verticalScale(50),
    paddingLeft: 10,
    textAlign: 'center'
  },
  block: {
    padding: 10,
  },
  blockTxt:{
    color: 'white',
    textAlign: 'center',
    fontFamily: 'dosis_bold',
  },
  value: {
    fontSize: 22,
  },
  key: {
    fontSize: 15
  },
  btnContainer:{
    justifyContent: 'center',
    alignItems: 'center'
  },
  followBtn: {
    borderColor: 'white',
    borderWidth: 1,
  }

})
const mapDispatchToProps = (dispatch) => {
  return{
    changeView: (view) => dispatch(changeView(view)),
    alert: (message) => dispatch(alert(message)),
  }
};
const mapStateToProps = ({nav,user}) => ({
  user,
  nav
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);
