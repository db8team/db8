import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Animated,
  ScrollView,
  Dimensions,
  Text,
  StyleSheet,
  Image
} from 'react-native'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import Colors from 'DB8/assets/colors';
import { 
  ifIphoneX, 
  getStatusBarHeight 
} from 'react-native-iphone-x-helper'
import RF from "react-native-responsive-fontsize"
import {Feather, MaterialIcons} from '@expo/vector-icons';
import { Button } from 'react-native-elements'
import BottomNavSpacer from 'DB8/components/nav/bottomNavSpacer'
import Header from 'DB8/components/common/header'
import SafeAreaView from 'DB8/components/elements/SafeAreaView'
import { Divider } from 'react-native-elements';
import {
  signOut
} from 'DB8/actions/users.actions'

const {height, width} = Dimensions.get('window');

class Settings extends Component {

  constructor(props){
    super(props)
    this.state = {
      profile: this.props.user,
      sideBarVissible: false
    }
  }
  componentDidMount() {
      console.log(this.state.profile.pic_ur+'?height=130')
  }
  
  render() {      
    return (
      <SafeAreaView style={styles.container}>
        <Header text={'Settings'}/>
        <ScrollView style={styles.container}>
          <Button 
            type="clear"
            onPress={() => this.props.changeView('settings')}
            title="Account Settings"
            buttonStyle={styles.btn}
            titleStyle={styles.titleStyle}
            icon={
              <Feather name="user" size={RF(3)}  />
            }
          />
          <Button 
            type="clear"
            onPress={() => this.props.changeView('settings')}
            title="Privacy & Security"
            buttonStyle={styles.btn}
            titleStyle={styles.titleStyle}
            icon={
              <Feather name="lock" size={RF(3)}  />
            }
          />
          <Button 
            type="clear"
            onPress={() => this.props.changeView('settings')}
            title="Notifications"
            buttonStyle={styles.btn}
            titleStyle={styles.titleStyle}
            icon={
              <Feather name="bell" size={RF(3)}  />
            }
          />
          <Divider style={styles.divider}/>
          
          <Button 
            type="clear"
            onPress={() => this.props.changeView('settings')}
            title="Help"
            buttonStyle={styles.btn}
            titleStyle={[styles.titleStyle]}
            icon={
              <Feather name="help-circle" size={RF(3)}  />
            }
          />
          <Button 
            type="clear"
            onPress={() => this.props.changeView('settings')}
            title="About"
            buttonStyle={styles.btn}
            titleStyle={[styles.titleStyle]}
            icon={
              <Feather name="info" size={RF(3)}  />
            }
          />
          <Divider style={styles.divider}/>
          <Button 
            type="clear"
            onPress={() => this.props.signOut()}
            title="Log out"
            buttonStyle={styles.btn}
            titleStyle={[styles.titleStyle, {color: Colors.defaultBlue}]}
            icon={
              <Feather name="log-out" size={RF(3)}  />
            }
          />
          
        </ScrollView>
        <BottomNavSpacer />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container:{
    flex: 1
  },

  titleStyle:{
    paddingLeft: 10,
    fontSize: RF(2.8),
    color: 'black',
    fontFamily: 'open_sans'
  },
  btn:{
    justifyContent: 'flex-start',
    paddingLeft: 15,
  },
  divider:{
    marginLeft: 15,
    marginRight:15
  }

})
const mapDispatchToProps = (dispatch) => {
  return{
    signOut:() => dispatch(signOut()),
    changeView: (view) => dispatch(changeView(view))
  }
};
const mapStateToProps = ({ user}) => ({
  user,
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Settings);
