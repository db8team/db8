import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { 
  StyleSheet, 
  Text, 
  View,  
  Dimensions,
  Image,
  TouchableOpacity
} from 'react-native';
import Colors from 'DB8/assets/colors';
import { 
  GiftedChat, 
} from 'react-native-gifted-chat';

import RF from "react-native-responsive-fontsize"
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { Button } from 'react-native-elements'

const {width, height} = Dimensions.get('window')

export class DebateView extends Component {
  state={
    messages: []
  }

  componentDidMount() {
  }
  renderDay(props) {
    let currentMessage = props.currentMessage.createdAt
    let oldMessage = props.previousMessage.createdAt
    if (props.currentMessage.createdAt.toDate) {
      currentMessage = props.currentMessage.createdAt.toDate()
      props.currentMessage.createdAt = props.currentMessage.createdAt.toDate()
    }
    if (props.previousMessage.createdAt ) {
      
      if (props.previousMessage.createdAt.toDate) {
        oldMessage = props.previousMessage.createdAt.toDate()
      }
      if (!((oldMessage.getYear() === currentMessage.getYear()) && (oldMessage.getMonth() === currentMessage.getMonth()) && (oldMessage.getDate() === currentMessage.getDate()))) {
        return <Day {...props}/>
      }
    }else{
      return <Day {...props}/> 
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>  {this.props.question.text} </Text>
        </View>
        <View style={styles.chat}>
          <GiftedChat
            messages={this.props.messages}
            user={{
                _id: this.props.users[0].uid,
                name: this.props.users[0].display_name,
                avatar: this.props.users[0].pic_url
            }}
            showUserAvatar={true}
            minInputToolbarHeight={0}
            renderInputToolbar={() => {(<View style={{backgroundColor: 'red'}}/>)}}
          />
        </View>
        <View style={styles.interactable} >
          <View style={styles.votingBtns}>
            <Button 
              type="outline"
              containerStyle={{flex:1}}
              icon={
                <Image style={styles.profilePic}
                  source={{uri:this.props.users[0].pic_url+'?height=300'}}
                />
              }
              buttonStyle={styles.voteBtnStyle}
              titleStyle={styles.voteBtnTitleStyle}
              title={this.props.question[this.props.answers[this.props.users[0].uid]] + ' (534)'}
            />
            {this.props.users[1] &&
              <Button 
                type="outline"

                containerStyle={{flex:1}}
                icon={
                  <Image style={styles.profilePic}
                    source={{uri:this.props.users[1].pic_url+'?height=300'}}
                  />
                }
                buttonStyle={styles.voteBtnStyle}
                titleStyle={styles.voteBtnTitleStyle}
                title={this.props.question[this.props.answers[this.props.users[1].uid]] + ' (251)'}
              />
            }

          </View>
          <View style={styles.votingNumbers}>
            <Text style={styles.votingNumber}>Including Jhon Snow and others</Text>
            <Text style={styles.votingNumber}>Including Quin Cook and others</Text>
          </View>
          <View style={styles.commentsContainer}>
            <View style={styles.focusedComments}>
              <View style={styles.comment}>
                <TouchableOpacity>
                  <Text style={styles.commentName}> 
                    Alex Jones
                  </Text>
                </TouchableOpacity>
                <Text style={styles.commentText}> 
                  ALIENS ARE EVERYWHERE !!!
                </Text>
              </View>
              <View style={styles.comment}>
                <TouchableOpacity style={styles.profileLink}>
                  <Text style={styles.commentName}> 
                    Jhon Smith
                  </Text>
                </TouchableOpacity>
                <Text style={styles.commentText}> 
                  This is some whack shit This is some whack shit This is some whack shit This is some whack shit
                </Text>
              </View>
            </View>
            <View style={styles.viewCommentsBtn}>
              <TouchableOpacity>
                <Text style={styles.viewCommentsText}> View More Comments (303)</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    borderWidth: 1,
    borderColor: Colors.darkGrey,
    borderBottomColor: Colors.darkerGrey,
    borderRadius: 20,
    backgroundColor: 'white'
  },
  
  header:{
    borderTopLeftRadius:20,
    borderTopRightRadius:20,
    padding: 10
  },
  headerText: {
    textAlign: 'center',
    fontFamily: 'dosis_medium',
    fontSize: RF(2.),
    // color:'white'
  },
  chat:{
    flex: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: Colors.darkerGrey,
    borderBottomColor: 'transparent'
  },
  interactable:{
    borderBottomLeftRadius:20,
  },
  votingBtns: {
    borderBottomColor: Colors.darkerGrey,
    flexDirection: 'row'
  },
  profilePic:{
    height: verticalScale(17.5), 
    borderRadius: verticalScale(17.5)/2, 
    width: verticalScale(17.5),
  },
  voteBtnStyle: {
    borderColor: Colors.defaultBlue,
    height: verticalScale(35), 
    padding: 0
  },
  voteBtnTitleStyle:{
    paddingLeft: 5,
    fontSize: RF(2),
    lineHeight: verticalScale(17.5),
    color: Colors.defaultBlue
  },
  votingNumbers:{
    flexDirection: 'row',
    paddingTop:5,

  },
  votingNumber:{
    flex: 1,
    fontSize: RF(1.2),
    textAlign: 'center',
    color: Colors.blackish,
    paddingLeft: 5,
    paddingRight:5,
  },
  commentsContainer: {
    width: '100%',
    paddingTop: 7,
    // flex:0,
    // backgroundColor: 'red'
  },
  comment:{
    paddingLeft:  10,
    paddingRight: 10,
    paddingBottom: 5,
    flexDirection: 'row',
    width: '100%',
  },
  commentText: {
    paddingLeft: 10,
    fontSize: RF(1.7),
    flex:1,
  },
  commentName:{
    fontWeight: 'bold',
    fontSize: RF(1.7)
  },
  viewCommentsText:{
    fontSize: RF(1.7),
    color: Colors.defaultBlue,
    textAlign: 'center',
    paddingTop: 3,
    paddingBottom: 3,
  },
  profileLink:{

  }
}) 
const mapDispatchToProps = (dispatch) => {
    return {
    };
};

const mapStateToProps = ({}) => ({
});



export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DebateView);