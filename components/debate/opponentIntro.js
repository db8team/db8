/* eslint no-use-before-define: ["error", { "variables": false }] */
import React from 'react';
import PropTypes from 'prop-types';
import { 
  Platform, 
  StyleSheet,
  View,
  Text,
  Image,
  Animated
} from 'react-native';
import Colors from 'DB8/assets/colors'
import * as userConnectorAnimation from 'DB8/assets/animations/userConnector';
import Animation from 'DB8/components/common/animations';
import {Feather} from '@expo/vector-icons';
import RF from "react-native-responsive-fontsize";
import { Button } from 'react-native-elements'
export default class OpponentIntro extends React.Component {
  state={
    radius: new Animated.Value(0),
    borderRadius: new Animated.Value(0),
    grow: true,
  }

  componentDidUpdate(prevProps, prevState) {
      if (!prevProps.name && this.props.name) {

        Animated.parallel([
          Animated.timing(                  // Animate over time
            this.state.radius,            // The animated value to drive
            {
              toValue: 130,                   // Animate to opacity: 1 (opaque)
              duration: 1000,              // Make it take a while
            }
          ),                 // Animate over time
          Animated.timing(                  // Animate over time
            this.state.borderRadius,            // The animated value to drive
            {
              toValue: 65,                   // Animate to opacity: 1 (opaque)
              duration: 1000,              // Make it take a while
            }
          ),  
        ]).start()
        
      }
  }
  render() {
        /*<View style={styles.container}>
          <Text style ={styles.name}>{this.props.name} seems to disagree with you!</Text>
          <View>
            <View style={styles.imgContainer}>
              <Image style={{height: 130, borderRadius: 65, width: 130}}
                source={{uri: 'http://i.pravatar.cc/130'}}
              />
 
            </View>
          <Text style={styles.openingStatement}>How Would You Like To Start The DB8 ?
          </Text>
          </View>
        </View>*/
    let name = this.props.name
    if (name ) {
      textStyle = {...styles.name,color: 'transparent'}
    }else{
      textStyle = styles.name
    }
    return (
      <View style={{flex: 1}}>
        
        <View style={styles.container}>

          <Text style ={textStyle}>{'Connecting To Another User'.toUpperCase()}</Text>

          <View>
            <View style={styles.imgContainer}>
              <Animation 
                data={userConnectorAnimation} 
                animationProps={{resizeMode: 'cover'}} 
                containerStyle={{flex:0,height:210, width: 260, padding:null, top: 20}}
              />
            </View>           
          </View>
        </View>
        {!name ?
          <View style={styles.container}>
            <Feather color={Colors.defaultBlue} name="user" size={35} style={{top: 20}}/>
          </View>
          :
          <View style={styles.container}>
            <Text style ={styles.name}>{`${'Spondon Banday'} seems to disagree with you!`.toUpperCase()}</Text>
            <View>
              <View style={styles.imgContainer}>
                <Animated.Image style={{height: this.state.radius, borderRadius: this.state.borderRadius, width: this.state.radius}}
                  source={{uri: this.props.img}}
                />
              </View>
              <Text style={styles.openingStatement}>How Would You Like To Start The DB8 ?
              </Text>
            </View>
          </View>
        }

        
        
        
      </View>

    );
  }

}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left:0,
    right:0,
    bottom:0,
    // flex: 1
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    // backgroundColor: 'white',
    // paddingBottom: 50,
   },
   name: {
     fontSize: 18,
     paddingBottom:10,
     paddingLeft: 25,
     paddingRight: 25,
     textAlign: 'center',
     color: '#312E31',
     fontFamily: 'dosis_medium',
     fontSize: RF(2.8),
     color: Colors.defaultBlue
   },

   openingStatement:{
    paddingTop: 15,
    fontFamily: 'dosis_medium',
    textAlign: 'center',
    position: 'relative',
    fontSize: 18,

   },
   imgContainer:{
    height:140,
    justifyContent: 'center',
     alignItems: 'center',
   }
});

OpponentIntro.defaultProps = {
  img: null,
  name: null,
};

OpponentIntro.propTypes = {
  img: PropTypes.string,
  name: PropTypes.string,

};