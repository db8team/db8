import React, { Component } from 'react';
import Colors from 'DB8/assets/colors'
import { 
  
  Platform, 
  StyleSheet, 
  Text, 
  View, 
  Image,
  Dimensions,
  Animated,
  TouchableWithoutFeedback
} from 'react-native';
import { BlurView } from 'expo';
import { ifIphoneX, getStatusBarHeight } from 'react-native-iphone-x-helper'
import {StatusBar} from 'react-native';
import RF from "react-native-responsive-fontsize"
import { Button } from 'react-native-elements'
import {Feather} from '@expo/vector-icons';
import { connect } from 'react-redux';
import {
  goBack,
} from 'DB8/actions/nav.actions';
import Btn from 'DB8/components/elements/btn'
import { Overlay } from 'react-native-elements'
const {height, width} = Dimensions.get('window');
class ChatHeader extends Component {

  
  render() {
    let infoText = ''
    if (this.props.live ) {
      infoText = 'Live Debate'
    }else{
      infoText = 'Practice Debate'
    }
    if (this.props.displayName) {
      infoText += ` With ${this.props.displayName}`
    }
    return (
      <View style={styles.container} >
        <View style={styles.chatHeader}>
          {!this.props.live &&
            <View style={styles.backBtn}>
              <Button 
                type="clear" 
                onPress={this.props.goBack}
                icon={
                  <Feather name="arrow-left" size={RF(4)} color={Colors.defaultBlue} />
                }
              />
            </View>
          }
          <View style={styles.profilePicContainer} >
            {
              !this.props.img &&
              <View style={styles.iconContainer} >
                <Feather name="user" size={30} color={Colors.defaultBlue} />
              </View>
            }
            {this.props.img &&
              <Image style={styles.img} 
                source={{uri: this.props.img}}
              />
            }
          </View>
          <View style={styles.textContainer}>
            <Text style={styles.questionText}>{this.props.questionText}</Text>
            <Text style={styles.infoText}>{infoText} </Text>                
          </View>

          {this.props.timer &&
            <View style={styles.timerContainer}>
              
              <View style={styles.timer}>
                {this.props.timer}
              </View>
            </View>
          }
          <View style={styles.menuContainer} ref={this._menuButtonref}>
            <Btn 
              type={"clear"}
              buttonStyle={{padding: 0, paddingLeft: 5}}
              onPress={this.props.toggleOptions}
              icon={
                <Feather  name="more-vertical" size={RF(4)}  />
              }
            />
          </View>
       </View>      
     </View>
    );
  }
}
const styles = StyleSheet.create({
  
  container:{
    width: width,
    height: 60,
    zIndex:2,
    marginTop: Platform.select({
      ios: 0,
      android: StatusBar.currentHeight,
    }),
  },
  // Chat Header Components
  menuContainer: {
    justifyContent: 'center',
  },
  chatHeader:{
    borderColor: Colors.darkerGrey,
    borderBottomWidth: 0.5,
    paddingLeft:5,
    paddingRight:0,
    flexDirection: 'row',
    height:60,
  },
  backBtn: {
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  profilePicContainer:{
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  iconContainer:{
    backgroundColor: Colors.darkGrey,
    borderRadius: 20,
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',

  },
  img:{
    height: 40,
    width: 40,
    borderRadius: 20
  },
  textContainer:{
    justifyContent: 'center',
    alignItems: 'flex-start',
    flex: 1,
    padding: 10,
    paddingRight: 0,

  },
  questionText:{
    fontFamily: 'dosis_medium',
    color: Colors.black,
    textAlign: 'left',
    fontSize: RF(2)
  },
  infoText: {
    color: '#999999',
    fontSize: RF(1.5)
  },
  timerContainer:{
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 5,
    left:5,
  },
  

})

const mapStateToProp = ({user,debates}) => {
  return {
    user
  }
}

const mapDispatchToProps = dispatch => {
  return{
    goBack: () => dispatch(goBack()),

  }
}
export default connect(mapStateToProp,mapDispatchToProps)(ChatHeader);

