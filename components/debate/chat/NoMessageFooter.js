/* eslint no-use-before-define: ["error", { "variables": false }] */

import PropTypes from 'prop-types';
import React from 'react';
import { 
  Platform, 
  StyleSheet,
  View,
  Text,
  Image
} from 'react-native';
import Colors from 'DB8/assets/colors'
import * as userConnectorAnimation from 'DB8/assets/animations/userConnector';
import Animation from 'DB8/components/common/animations';
export default class NoMessageFooter extends React.Component {

  
  render() {
    return (
      <View style={styles.container}>
        <Text style ={styles.name}>{this.props.name} seems to disagree with you!</Text>
        <View>
          <View style={styles.imgContainer}>
            <Image style={{height: 130, borderRadius: 65, width: 130}}
              source={{uri: this.props.img}}
            />
            <Animation data={userConnectorAnimation}/>
          </View>
        <Text style={styles.openingStatement}>How Would You Like To Start The DB8 ?
        </Text>
        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left:0,
    right:0,
    bottom:0,
    // flex: 1
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'white',
    // paddingBottom: 50,
   },
   name: {
     fontSize: 18,
     paddingBottom:10,
     textAlign: 'center',
     color: '#312E31',
   },
   text: {
     fontSize: 18,
     textAlign: 'center',
     color: '#949194',
   },
   openingStatement:{
    paddingTop: 10,
   },
   imgContainer:{
    height:140,
    justifyContent: 'center',
     alignItems: 'center',
   }
});

NoMessageFooter.defaultProps = {
  img: null,
  name: null,
};

NoMessageFooter.propTypes = {
  img: PropTypes.string,
  name: PropTypes.string,

};