import React, { Component } from 'react';

import { 
  KeyboardAvoidingView,
  Platform, 
  StyleSheet, 
  Text, 
  View, 
  Alert,
  StatusBar
} from 'react-native';

import { 
  GiftedChat, 
  Day
} from 'react-native-gifted-chat';

import { Button } from 'react-native-elements'
import InputToolbar from 'DB8/components/debate/chat/inputContainer'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Colors from 'DB8/assets/colors'
import Composer from 'DB8/components/debate/chat/composer'
import Send from 'DB8/components/debate/chat/send'
import ChatHeader from 'DB8/components/debate/chat/chatHeader'
import NoMessageFooter from 'DB8/components/debate/chat/NoMessageFooter'
import {
  initListener, 
  sendMessages,
  endDebate
} from 'DB8/actions/debate.actions'
import CountDown from 'DB8/components/common/countDown';
import {  Entypo } from '@expo/vector-icons';
import _ from 'lodash';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import RF from "react-native-responsive-fontsize";


class Chat extends Component {
  state = {
    started: false,
    pausedAt: null,
    lastMsgSent: null,
    messages:[],
    keyboardOpen: false,
    keyboardHeight: null,
  }

  onEndByOthers = () => {
    Alert.alert(
      'DB8 Ended ',
      `DB8 is ended by ${this.props.debate.opponent.display_name}`,
      [
        {
          text: 'Ok',
          onPress: () => this.endDebate(false)
        },
        {cancelable: false}
      ]
    )
  }

  endDebate = (initiated = true) =>{
    if (this.state.started) {
      this.props.debate.unsubscribe()
      this.props.endDebate(this.props.debate,initiated)
    }
  }

  checkMessages = (messages) => {
    let newMessages = []
    messages.forEach(function(message) {
      if (message.text != '') {
        newMessages.push(message)
      }
    });
    this.props.sendMessages(this.props.debate,newMessages)
  }

  componentDidMount() {
    if (!this.props.debate.live){
      this.setState({
        started: true,
        messages: this.props.debate.messages
      })
    }
    if(this.props.debate.live && (this.props.debate.users.answerA[0].listening && this.props.debate.users.answerB[0].listening)){
      this.setState({
        started: true,
        messages: this.props.debate.messages
      })
    }

  }

  componentDidUpdate(prevProps, prevState) {

    if(!this.state.started && this.props.debate.live && (this.props.debate.users.answerA[0].listening && this.props.debate.users.answerB[0].listening)){
      console.log(this.props.debate.live)
      this.setState({
        started: true,
        messages: this.props.debate.messages
      })
    }
    
    if (!_.isEqual(prevProps.debate.messages, this.props.debate.messages)) {
      this.setState({
        messages: this.props.debate.messages
      })
    }
    if (this.props.debate.ended === true) {
      this.onEndByOthers()
    }
    
  }

  renderInputToolbar = (props) =>{
    return(
      <InputToolbar keyboardOpen={this.state.keyboardOpen}  {...props} />
    )
  } 
  renderChatFooter = (props) => {
    if (this.state.messages.length > 0) {
      return(
        <View style={styles.chatFooter}>
        </View>
      )
    }else{
      return(
        <NoMessageFooter live={false} img={this.props.debate.opponent.pic_url} name={this.props.debate.opponent.display_name} />
      )
    }
  }
  renderSend(props) {

    let containerStyle = {
      alignItems: 'center',
      justifyContent: 'center',
      flex:1,
      height: 64,
    }

    return( 

      <Send alwaysShowSend={true} containerStyle={containerStyle} {...props}>
        <Entypo  name="chevron-with-circle-right" size={30} color={Colors.defaultBlue} />
      </Send>
    )

  }
  renderComposer(props) {
    return(
      <Composer  textInputStyle={styles.textInputStyle} {...props}/>
    )
  }

  renderDay(props) {
    let currentMessage = props.currentMessage.createdAt
    let oldMessage = props.previousMessage.createdAt
    if (props.currentMessage.createdAt.toDate) {
      currentMessage = props.currentMessage.createdAt.toDate()
      props.currentMessage.createdAt = props.currentMessage.createdAt.toDate()
    }
    if (props.previousMessage.createdAt ) {
      
      if (props.previousMessage.createdAt.toDate) {
        oldMessage = props.previousMessage.createdAt.toDate()
      }
      if (!((oldMessage.getYear() === currentMessage.getYear()) && (oldMessage.getMonth() === currentMessage.getMonth()) && (oldMessage.getDate() === currentMessage.getDate()))) {
        return <Day {...props}/>
      }
    }else{
      return <Day {...props}/> 
    }
  }

  render() {
    let timer = null
    if (this.state.started ) {
      timer = <CountDown
        size={20}
        until={30*60 + (this.props.connectedAt - Math.floor(new Date().getTime()/1000))}
        onFinish={() => this.endDebate()}
        digitStyle={{backgroundColor: 'transparent', borderWidth: 0, borderColor: '#1CC625'}}
        digitTxtStyle={{color: Colors.defaultBlue,fontWeight:null, fontFamily: 'dosis_medium'}}
        timeLabelStyle={{color: 'red', }}
        separatorStyle={{color: Colors.defaultBlue}}
        timeToShow={['M', 'S']}
        pause={!this.props.connected}
        timeLabels={{m: null, s: null}}
        showSeparator
      />
    }else{
      timer = <CountDown
        size={RF(10)}
        until={30*60}
        digitStyle={{backgroundColor: 'transparent', borderWidth: 0, borderColor: '#1CC625'}}
        digitTxtStyle={{color: Colors.defaultBlue}}
        timeLabelStyle={{color: 'red', }}
        separatorStyle={{color: Colors.defaultBlue}}
        timeToShow={['M', 'S']}
        pause={true}
        timeLabels={{m: null, s: null}}
        showSeparator
      />
    }
    return (
      <View style={styles.container}>
        <ChatHeader 
          opponent={this.props.debate.opponent}
          questionText={this.props.debate.question.text} 
          timer={this.props.debate.type === 'live' ? timer: null} 
          displayName={this.props.debate.opponent.display_name} 
          toggleOptions={this.props.toggleOptions}
          img={this.props.debate.opponent.pic_url}
          live={this.props.debate.live}
        />

        <KeyboardAvoidingView style={styles.gc} behavior={ Platform.OS === 'android' ? 'padding' :  null}  >
          <GiftedChat
            keyboardShouldPersistTaps={'never'}
            messages={this.state.messages}
            onSend={newMessages => this.checkMessages(newMessages)}
            user={{
                _id: this.props.user.uid,
                name: this.props.user.display_name,
                avatar: this.props.user.pic_url
            }}
            renderInputToolbar={this.renderInputToolbar}
            renderDay={this.renderDay}
            renderComposer={this.renderComposer}
            renderSend={this.renderSend}
            renderChatFooter={this.renderChatFooter}
          />
        </KeyboardAvoidingView>
      </View>
    )
  }
}
const styles = StyleSheet.create({

  container:{
    flex:1,
  },

  chatFooter:{
    ...ifIphoneX({
      padding: 0
    }, {
      padding: Platform.select({
        ios: 8.5,
        android: 8.5,
      }),
    }),
  },
  gc:{
    flex:90,
    zIndex: 0,
  },
  textInputStyle:{
    backgroundColor: Colors.grey,
    borderRadius: 15,
    marginTop: Platform.select({
      ios: 6,
      android: 0,
    }),
    marginBottom: Platform.select({
      ios: 5,
      android: 3,
    }),
  },

})


const mapStateToProp = ({user,debates}) => {

  return {
    user
  }
}

const mapDispatchToProps = dispatch => {
  return{
    initListener: (debate,answer) => dispatch(initListener(debate,answer)),
    sendMessages: (debate, messages) => dispatch(sendMessages(debate,messages)),
    endDebate : (debate, initiated = true) => dispatch(endDebate(debate,initiated))
  }
}

export default connect(mapStateToProp,mapDispatchToProps)(Chat);
