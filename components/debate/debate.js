import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { 
  StyleSheet, 
  Text, 
  View,  
  AppState,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity
} from 'react-native';
import OpponentIntro from './opponentIntro';
import Chat from './chat/chat';
import ChatHeader from './chat/chatHeader'
import CountDown from 'DB8/components/common/countDown';
import _ from 'lodash';
import BottomNavBar from 'DB8/components/nav/bottomNavBar'
import RF from "react-native-responsive-fontsize";
import Colors from 'DB8/assets/colors';
import Btn from 'DB8/components/elements/btn'
import {AntDesign} from '@expo/vector-icons';
import { Divider } from 'react-native-elements';
import Alert from 'DB8/components/elements/alert'
import {
  initListener, 
  endDebate,
  stopListening
} from 'DB8/actions/debate.actions'
import {
  addDetailsToDebate
} from 'DB8/helpers/debates.helper'
import SafeAreaView from 'react-native-safe-area-view'
import { ifIphoneX, getStatusBarHeight } from 'react-native-iphone-x-helper'
import { BlurView } from 'expo';
const {width, height} = Dimensions.get('window')

class Debate extends Component {
  state = {
    connected: false,
    ableToConnect: false,
    appState: AppState.currentState,
    appWaiting: 0,
    connectedAt: null,
    showOptions: false,

    //Alert Options
    alertTitle: null,
    showAlert: false,
    alertText: false,
    alertFirstOption: null,
    alertOnFirstPress: null,
    alertSecondOption: null,

  }
  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
    if (this.props.activeDebate) {
      this.props.initListener(this.props.activeDebate, this.props.activeDebate.answer)
    } 
  }

  onEndPress = () => {
    this.setState({
      showOptions: !this.state.showOptions,
      showAlert: true,
      alertTitle: 'Ending The DB8',
      alertText: 'You are ending the debate prematurely. Are you sure you want to continue ?',
      alertFirstOption: 'Continue',
      alertSecondOption: 'Cancel',
      alertOnFirstPress: () => this.endDebate(),
    })
    // Alert.alert(
    //   'Ending The DB8',
    //   'You are ending the debate prematurely. Are you sure you want to continue ?',
    //   [
    //     {
    //       text: 'Cancel',
    //       onPress: () => console.log('Cancel Pressed'),
    //       style: 'cancel',
    //     },
    //     {text: 'Continue', onPress: () => this.endDebate()},
    //   ],
    //   {cancelable: false},
    // );
  }

  endDebate = (initiated = true) =>{

    this.props.activeDebate.unsubscribe()
    this.props.endDebate(this.props.activeDebate,initiated)
    
  }

  onReportPress = () => {
    // Alert.alert(
    //   `Report ${this.props.activeDebate.opponent.display_name}`,
    //   `Comming Soon`
    // )
    this.setState({
      showOptions: !this.state.showOptions,
      showAlert: true,
      alertTitle: `Report ${this.props.activeDebate.opponent.display_name}`,
      alertText: `Comming Soon`,
      alertFirstOption: 'Okay',
      alertOnFirstPress: () => this.setState({showAlert: false})
    })
  }

  toggleOptions = () => {
    this.setState({
      showOptions: !this.state.showOptions
    })
  }
  renderPopUpOptions () {
    return (
      <View style={styles.actionItems} >
        <Btn onPress={this.onEndPress} buttonStyle={styles.btn} titleStyle={styles.endBtnText} containerStyle={[styles.actionItem]}  title="END" type="clear" />
        <Divider  />
        <Btn onPress={this.onReportPress} buttonStyle={styles.btn} titleStyle={styles.reportBtnText} containerStyle={styles.actionItem} title="REPORT" type="clear" />
        <Divider  />
        <Btn onPress={this.toggleOptions} buttonStyle={styles.btn} titleStyle={styles.cancelBtnText} containerStyle={[styles.actionItem]}  title="CANCEL" type="clear" />
      </View>
    )
  }

  componentDidUpdate(prevProps, prevState) {
    let {activeDebate} = this.props

    let prevDebate = prevProps.activeDebate
    if (activeDebate) {
      if (activeDebate.type === 'live' ){
        if (activeDebate.status === 'paired' ){
          if (this.state.connected && !activeDebate.users[activeDebate.opAnswer][0].listening) {
            this.setState({
              connected: false,
            })
          }
          if (!this.state.connected && !this.state.ableToConnect  && activeDebate.users[activeDebate.opAnswer][0].listening) {
            this.setState({
              connected: true,
              ableToConnect: true,
              connectedAt: Math.floor(new Date().getTime()/1000)
            })
          }
          if (!this.state.connected  && activeDebate.users[activeDebate.opAnswer][0].listening) {
            this.setState({
              connected: true        
            })
          }
        }
      }
    }
  }
  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
    if (this.props.activeDebate && this.props.activeDebate.unsubscribe) {
      this.props.activeDebate.unsubscribe()
    }
  }

  _handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState === 'active' &&
      nextAppState.match(/inactive|background/) 
    ) {
      this.props.stopListening(this.props.activeDebate, this.props.activeDebate.answer)
    }
    if (this.state.appState != 'active' && nextAppState === 'active') {
      if (this.props.activeDebate) {

        this.props.initListener(this.props.activeDebate, this.props.activeDebate.answer)
      } 
    }
    this.setState({appState: nextAppState});
  }
  render() {
    if (this.props.activeDebate && this.props.activeDebate.status != 'pairing' && (this.props.activeDebate.type === 'practice' || this.state.ableToConnect &&  this.props.activeDebate.users[this.props.activeDebate.answer][0].listening)) {
      return (
        <View style={styles.container}>
          <SafeAreaView style={styles.container}>
            <Chat toggleOptions={this.toggleOptions} debate={this.props.activeDebate} connectedAt={this.state.connectedAt}  connected={this.state.connected}/>
          </SafeAreaView>
          {this.state.showOptions && 
            <BlurView tint="dark" intensity={80} style={[styles.overlayContainer, styles.blurViewContainer]}>
              <View style={{ flex: 1}}>
                <TouchableOpacity onPress={this.toggleOptions}>
                  <View style={{height, width}}/>
                </TouchableOpacity>
              </View>
              <View >
                {
                  this.renderPopUpOptions()
                }
              </View>
            </BlurView> 
          }
          {this.state.showAlert &&
            <Alert title={this.state.alertTitle} text={this.state.alertText} firstOption={this.state.alertFirstOption} onFirstOptionPress={this.state.alertOnFirstPress} secondOption={this.state.alertSecondOption} onSecondOptionPress={() => this.setState({showAlert: false})} />
          }
        </View>
      )

    }else if(this.props.activeDebate && this.props.activeDebate.type === 'practice'){
      return (
        <SafeAreaView style={styles.container}>
          <ChatHeader 
            questionText={this.props.activeDebate.question.text} 
            timer={null} 
          />
          <OpponentIntro />
        </SafeAreaView>
      )
    }else{
      return (
        <SafeAreaView style={styles.container}>
          <CountDown
            size={RF(9)}
            until={Math.ceil((this.props.questions.pairingTill.getTime() - new Date().getTime())/1000)}
            digitStyle={{backgroundColor: 'transparent', borderWidth: 0, borderColor: '#1CC625'}}
            digitTxtStyle={{color: Colors.defaultBlue,fontWeight:null, fontFamily: 'dosis_medium'}}
            separatorStyle={{color: Colors.defaultBlue}}
            timeToShow={['M', 'S']}
            timeLabels={{m: null, s: null}}
            showSeparator
          />
          <Text style={styles.matchInfo}> TILL PAIRING ENDS</Text>
          <OpponentIntro />
          <View style={styles.endBtnContainer}>
            <Btn 
              buttonStyle={{backgroundColor: Colors.red, borderRadius: 10, padding: 20, paddingTop: 10, paddingBottom: 10}}
              title="EXIT"
            />
          </View>
        </SafeAreaView>
      )
    }
    // else{
    //   return(
    //     <SafeAreaView style={styles.container} forceInset={{ bottom: 'never'}}>
    //       <DebateList />
    //       <BottomNavBar overlay={false}/>
    //     </SafeAreaView>
    //   )
    // }
  }
}


const styles = StyleSheet.create({

  container:{
    flex:1,
  },
  matchInfo:{
    fontSize: RF(5),
    fontFamily: 'dosis_medium',
    textAlign: 'center',
    lineHeight: RF(6)
  },
  endBtnContainer:{
    alignItems: 'center',
    paddingBottom: 25,
  },
  overlayContainer:{
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    flex: 1,
    justifyContent: 'center'
  },
  blurViewContainer:{

  },

  cancelContainer:{
    backgroundColor: 'white',
    borderRadius: 20,
    marginTop: 10,
    marginBottom: 10
  },
  // Action Items Component
  actionItems:{
    height: 'auto',
    width: width - 20,
    backgroundColor: 'white',
    borderRadius: 20,
    marginBottom: 10,
    marginLeft: 10,
  },
  actionItem:{
    
  },
  actionLeftItem:{
    borderColor: Colors.darkGrey,
    borderLeftWidth: 1
  },
  btn:{
    paddingTop: 15,
    paddingBottom: 15,
  },
  endBtnText:{
    color: Colors.red,
    fontSize: RF(2.2),
    fontFamily: 'roboto_bold',
  },
  reportBtnText:{
    color: Colors.red,
    fontSize: RF(2),
    fontFamily: 'roboto_bold',
  },
  cancelBtnText:{
    color: Colors.defaultBlue,
    fontSize: RF(2.2),
    fontFamily: 'roboto_bold',
  }
})

Debate.propTypes = {
  user: PropTypes.object.isRequired, 
}

const mapStateToProp = ({user,debates,questions}) => {
  let activeDebate = addDetailsToDebate(debates.activeDebate,user.uid)
  return {
    debates,
    activeDebate: activeDebate,
    user,
    questions
  }
}

const mapDispatchToProps = dispatch => {
  return{
    stopListening: (debate,answer) => dispatch(stopListening(debate,answer)),
    initListener: (debate,answer, inital = true) => dispatch(initListener(debate,answer, inital)),
    endDebate : (debate, initiated = true) => dispatch(endDebate(debate,initiated))
  }
}

export default connect(mapStateToProp,mapDispatchToProps)(Debate);
