import React, { Component, PropTypes } from 'react';
import { 
  Text, 
  View,
  StyleSheet,
  FlatList,
  Platform,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  Image
} from 'react-native';
import { connect } from 'react-redux';

import {
  getDebatesRequest,
  selectDebate
} from 'DB8/actions/debate.actions'
import {
  changeView,
} from 'DB8/actions/nav.actions'
import {
  addDetailsToDebate
} from 'DB8/helpers/debates.helper'
import { Button } from 'react-native-elements'
import {Feather} from '@expo/vector-icons';
import Colors from 'DB8/assets/colors'
import RF from "react-native-responsive-fontsize"
import BottomNavBar from 'DB8/components/nav/bottomNavBar'
import Loading from 'DB8/components/common/loading'
import Btn from 'DB8/components/elements/btn'
import Header from 'DB8/components/common/header'
import SafeAreaView from 'react-native-safe-area-view'
import { ifIphoneX } from 'react-native-iphone-x-helper'


const {width, height} = Dimensions.get('window')
class PracticeDebatesList extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
      let showLoading = !this.props.debates || !this.props.debates.length 
      this.props.getDebatesRequest(
        [
          {key: `usersArr`, comp: "array-contains", value: this.props.user.uid},
          {key: 'type', comp: "==", value: "practice"},
          {key: 'ended',comp:"==", value: false}
        ], null, showLoading
      )
    }

    renderRow = (itemObj) => {
      let debate = addDetailsToDebate(itemObj.item,this.props.user.uid)
      let {opponent} = debate
      if (opponent) {
        var {pic_url, display_name} = opponent
      }
      
      if (debate.live ) {
        infoText = 'Live Debate'
      }else{
        infoText = 'Practice Debate'
      }
      if (display_name) {
        infoText += ` With ${debate.opponent.display_name}`
      }
      return (
        <TouchableOpacity onPress={() => this.props.selectDebate(itemObj.item, debate.answer)} style={styles.row}>
          <View style={styles.profilePicContainer} >
            {
              !pic_url &&
              <View style={styles.iconContainer} >
                <Feather name="user" size={30} color={Colors.defaultBlue} />
              </View>
            }
            {pic_url &&
              <Image style={styles.img} 
                source={{uri: pic_url}}
              />
            }
          </View>
          <View style={styles.textContainer}>
            <Text style={styles.questionText}>{debate.question.text} - {debate.question[debate.answer]}</Text>
            <Text style={styles.infoText}>{infoText} </Text>                
          </View>
        </TouchableOpacity>
      )
    }
    renderFlatList(debates){
      return(
        <FlatList
          style={{flex: 1}}
          data={debates}
          renderItem={(item) => this.renderRow(item)}
          keyExtractor={(item, index) => item.id.toString()}
        />
      )
    }
    renderCreatePractice() {
      return(
        <View style={styles.createPracticeContainer}>
          <Btn 
            shape="circular"
            rad={RF(20)}
            buttonStyle={styles.createBtnStyle}
            onPress={()=> this.props.changeView('questions',{startPractice: true})}
            icon={
              <Feather name="plus" size={RF(15)} color={Colors.defaultBlue} />
            }
          />
          <Text style={styles.createText} > {'Start A Practice DB8'.toUpperCase()} </Text>
        </View>
      )
    }
    render() {
        return (
          <SafeAreaView  style={styles.container} forceInset={{ bottom: 'never'}}>
            <Header text={'Practice Debates'}/>
            <View style={styles.list}>
              {this.props.debates &&
                  this.props.debates.length ?
                    this.renderFlatList(this.props.debates)
                  :
                    this.renderCreatePractice()

              }
            </View>

          </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
  wrapper:{
    flex: 1,

  },
  container:{
    flex: 1,
    paddingTop: Platform.select({
      ios: 0,
      android: StatusBar.currentHeight,
    }),
  },
  header:{
    flex: 1,
    borderBottomWidth:1,
    borderColor: Colors.darkerGrey,
    position: 'relative',
    top: Platform.select({
      ios: 0,
      android: StatusBar.currentHeight,
    }),
    left:0,
    right: 0,
    flexDirection: 'row',
  },
  headerTextContainer:{
    flex: 1,
    justifyContent: 'center',

  },
  headerText:{
    textAlign: 'left',
    fontSize: RF(3.5),
    fontFamily: 'dosis_medium'
  },
  backBtn: {
    justifyContent: 'center',
  },
  list:{
    flex: 14,
    ...ifIphoneX({
      paddingBottom: 60
    }, {
      paddingBottom: 50
    }),
  },
  createPracticeContainer:{
    position: 'absolute',
    top: 0,
    left: 0,
    right:0,
    bottom: height/13,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20
  },
  createBtnStyle:{
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderColor: Colors.defaultBlue,
    borderWidth: 1,
    bottom: 10,

  },
  createText:{
    fontFamily: 'dosis_medium',
    fontSize: RF(4),
    textAlign: 'center'
  },
  flatList:{},
  row: {
    flex: 1,
    padding: 5,
    paddingLeft: 10,
    width: width,
    borderBottomWidth: 1,
    borderColor: Colors.darkGrey,
    flexDirection: 'row',

  },
  profilePicContainer:{
    flex: 0,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  iconContainer:{
    backgroundColor: Colors.darkGrey,
    borderRadius: 20,
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',

  },
  img:{
    height: 40,
    width: 40,
    borderRadius: 20
  },
  textContainer:{
    justifyContent: 'center',
    alignItems: 'flex-start',
    flex: 1,
    padding: 10,
    paddingRight: 0,

  },
  questionText:{
    fontFamily: 'dosis_bold',
    color: Colors.black,
    textAlign: 'left',
    fontSize: RF(2.11)
  },
  infoText: {
    color: '#999999',
    fontSize: RF(2)
  },
})
const mapStateToProp = ({user,debates}) => {

  return {
    debates: debates.debates,
    user
  }
}

const mapDispatchToProps = dispatch => {
  return{
    getDebatesRequest: (query,filter, showLoading = true) => dispatch(getDebatesRequest(query,filter,showLoading)),
    changeView: (view,props) => dispatch(changeView(view,props)),
    selectDebate: (debate,answer) => dispatch(selectDebate(debate,answer))
  }
}

export default connect(mapStateToProp,mapDispatchToProps)(PracticeDebatesList);
