import React, { Component } from 'react';
import { connect } from 'react-redux'

import { 
  StyleSheet, 
  Text, View,
  Dimensions,
  Image 
} from 'react-native';

import { Button } from 'react-native-elements';

// import { 
//   loginWithFacebook, 
//   loginWithGoogle
// } from 'DB8/utility/fire';

import { 
  loginWithFacebook, 
  loginWithGoogle
} from 'DB8/actions/users.actions';

import Colors from 'DB8/assets/colors';
import {FontAwesome} from '@expo/vector-icons';
import RF from "react-native-responsive-fontsize"

const {height, width} = Dimensions.get('window');
class Landing extends Component {
  constructor(props){
    super(props)

    this.state ={
      height,
      width
    }

  }
  render() {

    return (
      <View style={styles.container}>
        <View style={styles.imgContainer}>
          <Image  resizeMode={'center'} style={styles.logo} source={require('DB8/assets/DB8_BIG.png')}/>

        </View>
        <View style={styles.btnGroup}>

          <View style={[styles.btnContainer,styles.topBtnContainer]}>
            <Button
              title="Continue With Google"
              
              type="outline"
              containerStyle={styles.btnElement}
              buttonStyle={styles.btnStyle}
              titleStyle={styles.btnTextStyle}
              icon ={
                <FontAwesome style={styles.btnIcon} name="google" size={25} color={Colors.red} />
              }
              onPress={() => this.props.loginWithGoogle()}
               />
          </View>
          <View style={[styles.btnContainer, styles.bottomBtnContainer]}>
            <Button
            
              type="outline"
              containerStyle={styles.btnElement}
              buttonStyle={styles.btnStyle}
              titleStyle={styles.btnTextStyle}
              icon ={
                <FontAwesome style={styles.btnIcon} name="facebook-f" size={25} color={Colors.red} />
              }
            onPress={() => this.props.loginWithFacebook()}
            title='Continue With Facebook' />
          </View>         
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1
  },
  imgContainer:{
    flex:5,
    paddingLeft: width/10,
    paddingTop:  width/10
  },
  logo:{
    height: 8*width/10,
    width: 8*width/10,

  },
  btnGroup:{
    flex:5,
  },

  btnContainer:{
    flex:1,
    alignItems: 'center',
  },
  topBtnContainer:{
    justifyContent: 'flex-end',
    paddingBottom: 10,
  },
  bottomBtnContainer:{
    justifyContent:'flex-start',
    paddingTop:10
  },
  btnIcon:{
    paddingRight: 8,
    color:Colors.defaultBlue,

  },
  questionContainer:{
    flex:5
  },

  btnElement:{
    width: '65%',

  },
  btnStyle:{
    justifyContent: 'flex-start',
    borderColor: Colors.defaultBlue,
  },
  btnTextStyle:{
    color: Colors.defaultBlue,
    fontSize: RF(2)
  }
})

const mapStateToProp = () => {
  return {}
}

const mapDispatchToProps = dispatch => {
  return{
    loginWithFacebook: () => dispatch(loginWithFacebook()),
    loginWithGoogle:() => dispatch(loginWithGoogle())
  }
}

export default connect(mapStateToProp,mapDispatchToProps)(Landing)
