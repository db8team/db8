import React, { Component } from 'react';
import {StyleSheet, Text, View, ImageBackground,Platform } from 'react-native';
import {DangerZone} from 'expo';
const { Lottie } = DangerZone;
class Animation extends React.Component {
  state = {
    animation: this.props.data,
  };
  
  componentDidMount() {
      this._playAnimation();
  }
  componentDidUpdate(prevProps, prevState) {
      if (this.props.replayFactor != prevProps.replayFactor) {
        this._playAnimation();
      }
  }
  render() {
    let animationProps = {

    }
    if (Platform.OS === 'android') {
      animationProps = {
        resizeMode: 'cover'
      }
    }else{
      animationProps = {
        resizeMode: 'contain'
      }
    }

    if (this.props.animationProps) {
      animationProps = {
        ...animationProps,
        ...this.props.animationProps
      }
    }
    return (
      <View style={[styles.animationContainer,this.props.containerStyle]}>
        {true &&
          <Lottie
            ref={animation => {
              this.animation = animation;
            }}
            {...animationProps}
            source={this.state.animation}
          />}

      </View>
    );
  }
  
  _playAnimation = () => {

    this.animation.reset();
    this.animation.play();
    
  };
}

const styles = StyleSheet.create({
  animationContainer: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    padding:10,
    flex: 1,
  },
  buttonContainer: {
    paddingTop: 20,
  },
});

export default Animation