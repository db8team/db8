import React, { Component } from 'react';
import { StyleSheet, Text, View, Dimensions, Image } from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';
import Colors from 'DB8/assets/colors';
import {FontAwesome} from '@expo/vector-icons';
import { DotsLoader } from 'react-native-indicator'
import RF from "react-native-responsive-fontsize"
import * as Progress from 'react-native-progress';

const {height, width} = Dimensions.get('window');

class Loading extends Component {
  constructor(props){ 
    super(props)

    this.state ={
      height,
      width
    }

  }
  render() {

    // return (
    //   <View style={styles.container}>
    //     <View style={styles.imgContainer}>
    //       <Image  resizeMode={'center'} style={styles.logo} source={require('DB8/assets/DB8_BIG.png')}/> 
    //     </View>
    //     <DotsLoader size={RF(3)}/>
    //   </View>
    // );
    return (
      <View style={styles.container}>
        <View style={this.props.bottomNav ? styles.imgContainer : [styles.imgContainer,styles.withoutBottomNav]}>
          <Image  resizeMode={'center'} style={styles.logo} source={require('DB8/assets/DB8_BIG.png')}/> 
        </View>
        <Progress.Bar width={200} indeterminate={true} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    height,
    width,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  withoutBottomNav: {
    paddingBottom: 50
  },
  logo:{
    height: 6*width/10,
    width: 6*width/10,

  },
  animation:{
    flex:5,
  },

 
})


export default Loading
