
import React, { Component } from 'react';
import { 
  Alert, 
  Text, 
  View,
  StyleSheet,
  FlatList,
  Platform,
  StatusBar,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import Colors from 'DB8/assets/colors'
import RF from "react-native-responsive-fontsize"
import Btn from 'DB8/components/elements/btn'
import {Feather} from '@expo/vector-icons';
import { connect } from 'react-redux';
import {
  goBack,
} from 'DB8/actions/nav.actions'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
      return (
        <View style={[styles.header]}>
          <View style={styles.backBtn}>
            <Btn 
              type="clear" 
              buttonStyle={{paddingTop: 0, paddingBottom: 0,}}
              onPress={this.props.goBack}
              icon={
                <Feather name="arrow-left" size={RF(3.5)} color={Colors.defaultBlue} />
              }
            />
          </View>
          <View style={styles.headerTextContainer}>
            <Text style={styles.headerText}>{this.props.text}</Text>
          </View>
        </View>          
      );
    }
}
const styles = StyleSheet.create({
  wrapper:{
    flex: 1,
  },
  header:{
    height: verticalScale(40),
    borderBottomWidth:0.5,
    borderColor: Colors.darkerGrey,
    flexDirection: 'row',
  },
  headerTextContainer:{
    flex: 1,
    justifyContent: 'center',

  },
  headerText:{
    textAlign: 'left',
    fontSize: RF(3),
    fontFamily: 'dosis_medium'
  },
  backBtn: {
    justifyContent: 'center',
  },
})

const mapStateToProp = ({user,debates}) => {
  return {
    user
  }
}

const mapDispatchToProps = dispatch => {
  return{
    goBack: () => dispatch(goBack()),

  }
}

export default connect(mapStateToProp,mapDispatchToProps)(Header);

