import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  StyleSheet, 
  Text, 
  View,
  Dimensions,
  SafeAreaView,
  Platform,
  StatusBar
} from 'react-native';
import { Button } from 'react-native-elements'
import {
  getTime,
  getQuestions
} from 'DB8/actions/questions.actions'
import {
  changeNavColor
} from 'DB8/actions/nav.actions'

import Colors from 'DB8/assets/colors';
import Swiper from '@nart/react-native-swiper'
import QuestionsList from './questionsList';
import Animation from 'DB8/components/common/animations';
import CountDown from 'DB8/components/common/countDown';
import {Feather} from '@expo/vector-icons';
import RF from "react-native-responsive-fontsize"
import CountDownTillPairing from './countDownTillPairing'
import Carousel from 'react-native-snap-carousel';
import CountDownTillPairingClosed from './countDownTillPairingClosed'
import { 
  ifIphoneX, 
  getStatusBarHeight 
} from 'react-native-iphone-x-helper'
const {width, height} = Dimensions.get('screen')

class Questions extends Component {
    constructor(props) {
      super(props);
      this.state = {
        pairingNow: 'waiting',
        slide: props.startPractice,
      }
    }
    componentDidMount() {

      this.checkIfPairingNow()
      if ( !this.props.questions.loadedAt || ( new Date().getDate() != this.props.questions.loadedAt.getDate() || new Date().getMonth() != this.props.questions.loadedAt.getMonth() || new Date().getFullYear() != this.props.questions.loadedAt.getFullYear() ) ) {
        console.log('loadedAt', this.props.questions.loadedAt)
        this.props.getQuestions()
      }
      
      if (this.props.startPractice) {
        this.props.changeNavColor(false)
      }
    }
    componentDidUpdate(prevProps, prevState) {
      if (prevState.pairingNow === 'waiting' && this.props.questions.liveTime) {
        this.setState({
          pairingNow: false
        })
      }
      if (this.props.questions.liveTime) {
        this.checkIfPairingNow()
      }
    }
    checkIfPairingNow = () => {
      if (this.props.questions.liveTime && (new Date().getTime() > this.props.questions.liveTime.getTime())) {
        if (new Date().getTime() < this.props.questions.pairingTill.getTime()) {
          if (!this.state.pairingNow) {
            this.setState({
              pairingNow: true
            })
          }
        }else{
          this.props.getTime()
        }
      }else if(this.props.questions.liveTime && (new Date().getTime() < this.props.questions.liveTime.getTime())){
        if (this.state.pairingNow) {
          this.setState({
            pairingNow: false
          })
        }
      }else{
        this.props.getTime()
      }
    }
    onCountDownFinish = () => {
      setTimeout(() => {
        let pairingNow = !this.state.pairingNow
        this.setState({
          pairingNow
        })
      },1);
    }

    onSwipe = (slide) => {
      this.props.changeNavColor(!slide)
      this.setState({
        slide,
      })
    }
    

    renderCountDown = () => {
      return (
        <View style={styles.wrapper}> 
          <CountDownTillPairing
           pairingNow={this.state.pairingNow} 
           onFinish={this.onCountDownFinish} 
           slide={this.state.slide}
           time={this.state.pairingNow ? this.props.questions.pairingTill : this.props.questions.liveTime}  
           />    
        </View>
      )
    }

    renderPracticeList = () => {
      return(
            <QuestionsList type="practice"/>)
    }
    renderItems = ({item, index}) => {
      return(item())
      
    }
    render() {
      return(
        <View style={styles.container}>
          {!this.state.join ?
              <Carousel
                ref={(c) => { this._carousel = c; }}
                data={[this.renderCountDown, this.renderPracticeList]}
                renderItem={this.renderItems}
                itemHeight={height}
                sliderHeight={height}
                containerCustomStyle={styles.carouselContainer}
                vertical={true}
                onSnapToItem={this.onSwipe}
                firstItem={this.props.startPractice ? 1 : 0}
                inactiveSlideScale={1}
              />
            :
              <QuestionsList type="live" />
          }
        </View>
      )  
    }

}
const styles = StyleSheet.create({
  wrapper:{
    flex: 1,
    height
  },
  container: {
    flex: 1,
 
  },
  countDownContainer:{
    padding: 40,
    paddingLeft: 10,
    paddingRight: 10,
  },
  countDown:{
    justifyContent: 'center',
    alignItems: 'center',
  },
  matchInfo:{
    fontSize: RF(5),
    textAlign: 'center',
  },
  swipeUpContainer:{
    padding: 40,
  },
  swipeUpInstructions: {
    textAlign: 'center',
    fontSize: RF(4),
    color: Colors.defaultBlue,
    paddingTop: 5,
    fontFamily:'dosis-medium',

  },
  carouselContainer:{
    flexGrow:0,
  }
})




Questions.propTypes = {
  user: PropTypes.object.isRequired, 
}

const mapStateToProp = ({user,questions}) => {
  return {
    user,
    questions,
  }
}

const mapDispatchToProps = dispatch => {
  return{
    getTime: () => dispatch(getTime()),
    getQuestions: () => dispatch(getQuestions()),
    changeNavColor: (dark) => dispatch(changeNavColor(dark))
  }
}



export default connect(mapStateToProp,mapDispatchToProps)(Questions);
