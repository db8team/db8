import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  Animated,  
  StyleSheet, 
  Text, 
  View, 
  ImageBackground,
  Dimensions,
  StatusBar,
  Platform,
  TouchableOpacity
} from 'react-native';
import { Button } from 'react-native-elements'
import BottomNavBar from 'DB8/components/nav/bottomNavBar'
import _ from 'lodash'
import {
  getQuestions,
  getTime,
} from 'DB8/actions/questions.actions'
import {
  changeView,
} from 'DB8/actions/nav.actions'
import {
  pairRequest
} from 'DB8/actions/debate.actions'
import { LinearGradient } from 'expo';
import Swiper from '@nart/react-native-swiper'
import Carousel from 'react-native-snap-carousel';
import Colors from 'DB8/assets/colors';
import RF from "react-native-responsive-fontsize"
import {Feather, AntDesign} from '@expo/vector-icons';
import { 
  ifIphoneX, 
  getStatusBarHeight 
} from 'react-native-iphone-x-helper'



const {width, height} = Dimensions.get('window')
class QuestionsList extends Component {

    state={
      questions: [],
      categories: [],
      categoryIndex: 0,
    }

    componentDidMount() {
      if ( !this.props.questions.loadedAt || ( new Date().getDate() != this.props.questions.loadedAt.getDate() || new Date().getMonth() != this.props.questions.loadedAt.getMonth() || new Date().getFullYear() != this.props.questions.loadedAt.getFullYear() ) ) {
        this.props.getQuestions()
      }else if(this.props.questions[this.props.type]){
        this.loadQuestions()
      }

      
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.questions.loadedAt != this.props.questions.loadedAt) {
          this.loadQuestions()
        }
    }

    loadQuestions(){
      this.setState({
        questions: this.props.type === 'live'? this.props.questions.live.all : this.props.questions.practice,
        catgories: Object.keys(this.props.questions.live),
        live: this.props.questions,
        practice: this.props.questions

      })
    }
    renderQuestion(question){
      if (question && question.img) {
        return(
          <ImageBackground 
            resizeMode={'cover'} 
            source={{uri: question.img}}
            imageStyle={{width:'100%',height:'100%'}} 
            style={styles.container}
       
            key={question.text}
          >
            <View style={styles.imageOverlay} >
              {this.renderInteractables(question)}
            </View>
          </ImageBackground>
          )
      }else{
        return(
          <View 
            style={styles.container}
            key={question.text}
          >
            {this.renderInteractables(question)}
            
          </View>
        )
      }
    }

    renderLoading(){
      return(
        <View 
          style={styles.container}
        >         
          
        </View>
      )
    }
    renderQuestions(questions){
      let components = questions.map(q => this.renderQuestion(q))
      return components
    }

    renderInteractables(question) {

      return(
   
        <View style={styles.container}>
          <View style={styles.questionTextContainer}>
            <View style={styles.textWrapper}>
              <Text style={styles.questionText}>{question.text}</Text>
            </View>
          </View>
          <View style={styles.questionActionItems}>
            <View style={styles.btnsGroupContainer}>     
              <Button 
                titleStyle={styles.btnText} 
                buttonStyle={styles.btn} 
                type={'outline'} 
                raised={true} 
                onPress={() => this.props.pair(question,"answerA",this.props.type,this.props.questions.pairingTill)}  
                title={question.answerA}
              />
              <Button 
                titleStyle={styles.btnText} 
                buttonStyle={styles.btn} 
                type={'outline'} 
                raised={true} 
                onPress={() => this.props.pair(question,"answerB",this.props.type,this.props.questions.pairingTill)}  
                title={question.answerB}
              />
            </View>
            <View style={styles.swipeInstructionContainer}>

              <Text style={styles.swipeInstructionText}>
                Swipe To Change Questions
              </Text>
            </View>
          </View>
        </View>   
      )
    }

    onSlideChange = index => {
      if (index === this.state.questions.length) {
        this.props.getQuestions()
        this.setState({
          questions: []
        })
      }
    }
    renderPracticeList(){
      if (this.props.requestingNow) {
        return(this.renderLoading())
      }else{
        return(
          <LinearGradient 
            colors={Colors.blueGradient} 
            style={styles.container}
          > 
            
            <Swiper 
              // style={styles.wrapper} 
              onIndexChanged={index => this.onSlideChange(index)}
              showsPagination={false} 
              showsButtons={false}
            >
              {this.renderQuestions(this.state.questions)}

            </Swiper>
            {this.props.type === "practice" &&
              <View style={styles.swipeDownContainer}>
                  <Feather 
                    style={{textAlign: 'center'}} 
                    color="white"  
                    name="chevrons-down" 
                    size={RF(4)}  
                  />
                </View>
            }
          </LinearGradient>
        )  
      }   
    }
    onBeforeSnapToItem = (slideIndex) => {
      let categories = Object.keys(this.props.questions.live)
      this.setState({
        questions: this.props.questions.live[categories[slideIndex]],
        categoryIndex: slideIndex
      })
    }
    renderCategory = ({item, index}) => {
      if (this.state.categoryIndex === index) {
        return(
          <View style={styles.categoryContainer}>
              <Text style={styles.categoryText}>{item.toUpperCase()}</Text>
          </View>
        ) 
      }else{
        return(
          <TouchableOpacity onPress={() => this.onCategoryPress(index)} style={styles.categoryContainer}>
              <Text style={styles.categoryText}>{item.toUpperCase()}</Text>
          </TouchableOpacity>
        )      
      }
    }
    onCategoryPress = (index) => {

      this._carousel.snapToItem(index)
    }
    renderLiveLists(){
      if (this.props.requestingNow) {
        return(this.renderLoading())
      }else{
        return(
          <LinearGradient 
            colors={Colors.blueGradient} 
            style={styles.container}
          > 

            <Swiper
              onIndexChanged={index => this.onSlideChange(index)}
              showsPagination={false} 
              loop={false}
              showsButtons={false}
            >

              {this.renderQuestions(this.state.questions)}

            </Swiper>

            <Carousel
              ref={(c) => { this._carousel = c; }}
              data={Object.keys(this.props.questions.live)}
              renderItem={this.renderCategory}
              itemWidth={200}
              sliderWidth={width}
              itemHeight={200}
              onBeforeSnapToItem={this.onBeforeSnapToItem}
              containerCustomStyle={styles.carouselContainer}


            />
          </LinearGradient>
        )
      }
    }
    render() {
      if (this.props.type === 'practice') {
        return(this.renderPracticeList())
      }else{
        return(
          this.renderLiveLists()
        )  
      } 
    }
}
const styles = StyleSheet.create({
  wrapper:{
    flex: 1,
  },
  nextQSwipe:{
    backgroundColor: 'red',
    justifyContent: 'center', 
    alignItems: 'center',
    flex:1,
  },

  questionText:{
    fontSize:40,
    color: 'white',
    textAlign: 'center',
    fontFamily: 'dosis_medium'
  },

  questionTextContainer:{
    justifyContent: 'center', 
    alignItems: 'center',
    flex:1.8,

  },

  questionTextImgContainer:{
    height: '100%',
    width: '100%',
    justifyContent: 'center', 
    alignItems: 'center',
    fontSize:40,
    opacity:5

  },

  questionActionItems:{
    flex:1,
  },

  container:{
    flex:1,
  },

  btnsGroupContainer:{
    flexDirection: 'row',    
    backgroundColor: 'transparent',
    justifyContent: 'space-evenly',
  },

  swipeInstructionContainer:{
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 30,
  },

  swipeInstructionText:{
    color: 'white'
  },

  btnContainer:{},

  nextQuestionTextContainer:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },

  textWrapper:{
    padding: 15
  },

  imageOverlay:{
    backgroundColor: Colors.darkerGreyOpacity,
    flex:1,
  },

  btn:{
    borderColor: 'white',
    width: RF(15),
    height: RF(15),
    borderRadius:  RF(15)/2,
  },

  btnText:{
    color:Colors.defaultBlue,
    fontSize: RF(1.9)
  },

  swipeDownContainer:{
    position: 'absolute',
    top: getStatusBarHeight() + 5,
    left: 0,
    right: 0,
  },

  categoryContainer: {
    padding: 20,
    borderRadius: 15,
    borderWidth: 2,
    borderColor: Colors.white,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },

  categoryText:{ 
    textTransform: 'uppercase',
    fontFamily: 'dosis_medium',
    color: Colors.white,
    fontSize: RF(3)
  },

  carouselContainer:{
    flexGrow:0,
    ...ifIphoneX({
      paddingTop: 50

    }, {
      paddingTop: getStatusBarHeight() + 10
    }),
    position: 'absolute',
    top:0
  }

})




QuestionsList.propTypes = {
  user: PropTypes.object.isRequired, 
}

const mapStateToProp = ({user,questions}) => {
  return {
    questions,
    user,
  }
}

const mapDispatchToProps = dispatch => {
  return{
    getQuestions: () => dispatch(getQuestions()),
    pair:  (question, answer,type,till) => dispatch(pairRequest(question, answer,type,till)),
    changeView: (view) => dispatch(changeView(view))
  }
}

export default connect(mapStateToProp,mapDispatchToProps)(QuestionsList);
