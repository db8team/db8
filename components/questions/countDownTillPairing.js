import React, { Component } from 'react';
import {
  StyleSheet, 
  Text, 
  View,
  SafeAreaView,
  Platform,
  Dimensions,
  StatusBar
} from 'react-native';
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Animation from 'DB8/components/common/animations';
import * as hourGlassAnimation from 'DB8/assets/animations/hourGlass';
import * as boxingAnimation from 'DB8/assets/animations/boxing';
import CountDown from 'DB8/components/common/countDown';
import {Feather} from '@expo/vector-icons';
import RF from "react-native-responsive-fontsize";
import Colors from 'DB8/assets/colors';
import BottomNavBar from 'DB8/components/nav/bottomNavBar'
import delay from 'DB8/helpers/delay'
import Btn from 'DB8/components/elements/btn';
import Loading from 'DB8/components/common/loading'
import {
  changeView,
} from 'DB8/actions/nav.actions'
import { 
  ifIphoneX, 
  getStatusBarHeight 
} from 'react-native-iphone-x-helper'

const {width, height} = Dimensions.get('window')


class CountDownTillPairing extends Component {
    state={
      timeToShow: null,
      start: false,
    }
    componentDidMount() {
      if (this.props.time) {
        this.initCountDown()
      }
    }
    componentDidUpdate(prevProps, prevState) {
      if (!prevProps.time && this.props.time) {
        this.initCountDown()
      }
    }
    initCountDown = async () => {
      let now = new Date().getTime()
      let awaitTime = ((now/1000)%1)*1000
      await delay(Math.round(awaitTime))
      this.setState({
        start: true,
        timeToShow: this.caclTimeToShow(Math.floor((this.props.time.getTime() - new Date().getTime())/1000))
      })
      
    }
    caclTimeToShow = (time) => {
      if (time <= 3600) {
        return['M', 'S']
      }else{
        return ['H','M', 'S']
      }
    }
    onChange = (time) => {
      let timeToShow = this.caclTimeToShow(time)
      if (this.state.timeToShow !== timeToShow) {
        this.setState({
          timeToShow
        })
      }
      if (time < 0) {
        this.props.onFinish()
      }
    }
    render() {
        let {onFinish, pairingNow} = this.props
        
          return (
            <SafeAreaView style={styles.wrapper}>

              <View style={styles.countDownContainer}>
                {this.state.start ?

                  <CountDown
                    size={RF(9)}
                    until={Math.ceil((this.props.time.getTime() - new Date().getTime())/1000)}
                    onFinish={onFinish}
                    onChange={this.onChange}
                    style={styles.countDown}
                    digitStyle={{backgroundColor: 'transparent', borderWidth: 0, borderColor: '#1CC625'}}
                    digitTxtStyle={{color: Colors.defaultBlue,fontWeight:null, fontFamily: 'dosis_medium'}}
                    separatorStyle={{color: Colors.defaultBlue}}
                    timeToShow={this.state.timeToShow}
                    timeLabels={{m: null, s: null}}
                    showSeparator
                  />
                  :
                  <CountDown
                    size={RF(9)}
                    until={0}
                    style={styles.countDown}
                    digitStyle={{backgroundColor: 'transparent', borderWidth: 0, borderColor: '#1CC625'}}
                    digitTxtStyle={{color: Colors.defaultBlue,fontWeight:null, fontFamily: 'dosis_medium'}}
                    separatorStyle={{color: Colors.defaultBlue}}
                    timeToShow={['M', 'S']}
                    timeLabels={{m: null, s: null}}
                    showSeparator
                  />
                
                }
                <Text style={styles.matchInfo}> TILL PAIRING {pairingNow ? 'ENDS': 'STARTS'}</Text>
              </View>
              {pairingNow &&
                <View style={styles.animationBoxingWrapper}>
                  <Animation replayFactor={this.props.slide} data={boxingAnimation}/>
                </View>
              }
              {!pairingNow &&
                <View
                  style={styles.animationHourGlassWrapper}
                >
                  <Animation replayFactor={this.props.slide} data={hourGlassAnimation}/>
                </View>
              }
              <View style={styles.test}>
                {pairingNow &&
                  <View style={styles.btnWrapper}>
                    <Btn onPress={() => this.props.changeView('liveQuestions')} title="JOIN" titleStyle={styles.btnText}  buttonStyle={styles.btn} />
                  </View>

                }
                
                <View style={styles.swipeUpContainer}>
                  <Feather style={{textAlign: 'center'}}  name="chevrons-up" size={RF(4)}  />
                  <Text style={styles.swipeUpInstructions}> SWIPE UP TO PRACTICE </Text>
                </View>
              </View>
            </SafeAreaView> 
          );
        // }else{
        //   return(
        //     <Loading />
        //   )
        // }
        
    }
}

const calcAndroidHeightOfAnimation = () => {
  if (getStatusBarHeight() > 25) {
    return height - RF(9)*1.5 - RF(6) - RF(4.7) - RF(4) - 55.3 - RF(3.5) - 43.3
  }else{
    return height - RF(9)*1.5 - RF(6) - RF(4.7) - RF(4) - 55.3 - 2*getStatusBarHeight() - RF(3.5) - 43.3
  }
}
const styles = StyleSheet.create({
  test: {
    bottom: 0,
  },
  wrapper:{
    flex: 1,
  },
  container: {
    flex: 1,
  },
  hidden:{
    height: 0,
    width: 0,
    flex: 0,
  },
  animationHourGlassWrapper:{  
    ...ifIphoneX({
      height: height - RF(9)*1.5 - RF(6) - RF(4.7) - RF(4) - 80.3 - getStatusBarHeight(),
    }, {
      height: Platform.select({
        android: height - RF(9)*1.5 - RF(6) - RF(4.7) - RF(4) - 60.3 - 2*getStatusBarHeight(),
        ios: height - RF(9)*1.5 - RF(6) - RF(4.7) - RF(4) - 55.3 - getStatusBarHeight(),
      })
    }),
  },
  animationBoxingWrapper:{
    height: 500,
    ...ifIphoneX({
      height: height - RF(9)*1.5 - RF(6) - RF(4.7) - RF(4) - 80.3 - getStatusBarHeight() - RF(3.5) - 43.3,
    }, {
      height: Platform.select({
        android: calcAndroidHeightOfAnimation(),
        ios: height - RF(9)*1.5 - RF(6) - RF(4.7) - RF(4) - 55.3 - getStatusBarHeight() - RF(3.5) - 43.3,
      })
    }),
  },
  btnWrapper:{
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  },
  btn:{
    backgroundColor: Colors.defaultBlue,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 8,
    paddingBottom: 8,

  },
  btnText:{
    color: Colors.white,
    fontFamily: 'dosis_medium',
    fontSize: RF(2.5),
    lineHeight: RF(3.5)
  },
  countDownContainer:{
    paddingTop: Platform.select({
      ios: 0,
      android: StatusBar.currentHeight
    }),
    paddingLeft: 10,
    paddingRight: 10,
    zIndex: 2,
  },
  countDown:{
    justifyContent: 'center',
    alignItems: 'center',
  },
  matchInfo:{
    fontSize: RF(5),
    fontFamily: 'dosis_medium',
    textAlign: 'center',
    lineHeight: RF(6)
    // color: Colors.defaultBlue,
  },
  swipeUpContainer:{
    paddingBottom: 70,
    justifyContent: 'flex-end',
  },
  swipeUpInstructions: {
    textAlign: 'center',
    fontSize: RF(3.7),
    lineHeight: RF(4.7),
    color: Colors.defaultBlue,
    fontFamily:'dosis_medium',

  },
})

CountDownTillPairing.propTypes = {
  user: PropTypes.object.isRequired, 
}
const mapStateToProp = ({user,questions}) => {
  return {
    user,
  }
}

const mapDispatchToProps = dispatch => {
  return{
    changeView: (view) => dispatch(changeView(view))
  }
}

export default connect(mapStateToProp,mapDispatchToProps)(CountDownTillPairing);

