import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import {
  StyleSheet, 
  Text, 
  View,
  SafeAreaView,
  Platform,
  Dimensions,
} from 'react-native';
import Animation from 'DB8/components/common/animations';
import * as hourGlassAnimation from 'DB8/assets/animations/hourGlass';
import CountDown from 'DB8/components/common/countDown';
import {Feather} from '@expo/vector-icons';
import RF from "react-native-responsive-fontsize";
import Colors from 'DB8/assets/colors';
import Btn from 'DB8/components/elements/btn';
import * as boxingAnimation from 'DB8/assets/animations/boxing';
import {
  changeView,
} from 'DB8/actions/nav.actions'
import BottomNavBar from 'DB8/components/nav/bottomNavBar'

const {width, height} = Dimensions.get('window')

class CountDownTillPairingClosed extends Component {
    state={
      timeToShow: null,
      time: null
    }
    componentDidMount() {
      if (this.props.time) {
        this.initCountDown()
      }
    }
    componentDidUpdate(prevProps, prevState) {
        if (!prevProps.time && this.props.time) {
          this.initCountDown()
        }
    }
    initCountDown = () =>{
      let time = Math.round((this.props.time.getTime() - new Date().getTime())/1000)
      this.setState({
        time,
        timeToShow: this.caclTimeToShow(time)
      })
      
    }
    caclTimeToShow = (time) => {
      if (time <= 3600) {
        return['M','S']
      }else{
        return ['H','M','S']
      }
    }
    onChange = (time) => {
      let timeToShow = this.caclTimeToShow(time)
      if (this.state.timeToShow !== timeToShow) {
        this.setState({
          timeToShow
        })
      }
    }
    render() {
        let {onFinish, pairingNow} = this.props
        let timeToShow = this.state.timeToShow
        let time = this.state.time

        if (time ) {

          return (
            <SafeAreaView style={pairingNow ?  styles.wrapper : [styles.wrapper,styles.hidden]}>
      
              <View style={styles.countDownContainer}>
                {time && 
                  <CountDown
                    size={pairingNow? RF(9) : 0}
                    until={time}
                    onFinish={onFinish}
                    style={styles.countDown}
                    digitStyle={{backgroundColor: 'transparent', borderWidth: 0, borderColor: '#1CC625'}}
                    digitTxtStyle={{color: Colors.defaultBlue, fontWeight:null,fontFamily: 'dosis_medium'}}

                    separatorStyle={{color: Colors.defaultBlue}}
                    timeToShow={this.state.timeToShow}
                    timeLabels={{m: null, s: null}}
                    showSeparator
                  />
                }
                <Text style={styles.matchInfo}> TILL MATCHES CLOSE </Text>
              </View>
              <View style={styles.animationWrapper}>
                <Animation androidStyle={{height: 400, width:400}}  data={boxingAnimation}/>
              </View>
              <View style={styles.btnWrapper}>
                <Btn onPress={() => this.props.changeView('liveQuestions')} title="JOIN" titleStyle={styles.btnText}  buttonStyle={styles.btn} />
              </View>
              <View style={styles.swipeUpContainer}>
                <Feather style={{textAlign: 'center'}}  name="chevrons-up" size={RF(4)}  />
                <Text style={styles.swipeUpInstructions}> SWIPE UP TO PRACTICE </Text>
              </View>
              <BottomNavBar overlay={true} colorScheme={"dark"}/>
            </SafeAreaView> 
          );
        }else{
          return(
            <View></View>
            )
        }
        
    }
}
const styles = StyleSheet.create({
  wrapper:{
    flex: 1,
  },
  container: {
    flex: 1,
  },
  hidden:{
    height: 0,
    width: 0,
    flex:0,
  },
  animationWrapper:Platform.select({
    android:{
      height: 330,
      width:width,
      marginTop: -30,
      zIndex:1,
    },
    ios: {
      flex:1
    }
  }),
  btnWrapper:Platform.select({
    android:{
      justifyContent: 'center',
      alignItems: 'center',
      zIndex:2,
      marginTop: 10,
    },
    ios: {
      justifyContent: 'center',
      alignItems: 'center',
    }
  }),
  btn:{
    backgroundColor: Colors.defaultBlue,
    paddingLeft: 20,
    paddingRight: 20,

  },
  btnText:{
    color: Colors.white,
    fontFamily: 'dosis_medium',
    fontSize: 20,
  },
  countDownContainer:{
    padding: 30,
    paddingLeft: 10,
    paddingRight: 10,
    zIndex:2,
  },
  countDown:{
    justifyContent: 'center',
    alignItems: 'center',
  },
  matchInfo:{
    fontSize: RF(5),
    textAlign: 'center',
    fontFamily: 'dosis_medium',
    // color: Colors.defaultBlue,
  },
  swipeUpContainer:Platform.select({
    android:{
      position: 'absolute',
      bottom: 60,
      left:0,
      right:0,
    },
    ios: {
      justifyContent: 'center',
      alignItems: 'center',
      padding: 20,
      paddingBottom: 65,
    }
  }),
  swipeUpInstructions: {
    textAlign: 'center',
    fontSize: RF(4),
    color: Colors.defaultBlue,
    paddingTop: 5,
    fontFamily:'dosis_medium',

  },
})
CountDownTillPairingClosed.propTypes = {
  user: PropTypes.object.isRequired, 
}
const mapStateToProp = ({user,questions}) => {
  return {
    user,
  }
}

const mapDispatchToProps = dispatch => {
  return{
    changeView: (view) => dispatch(changeView(view))
  }
}

export default connect(mapStateToProp,mapDispatchToProps)(CountDownTillPairingClosed);

