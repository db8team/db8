import React, { Component } from 'react';
import { 
  Animated,
  Text, 
  View,
  StyleSheet,
  Dimensions
} from 'react-native';
import { BlurView } from 'expo';
import { 
  ifIphoneX, 
  getStatusBarHeight 
} from 'react-native-iphone-x-helper'

import Colors from 'DB8/assets/colors';
import RF from "react-native-responsive-fontsize"
import {AntDesign, Feather} from '@expo/vector-icons';
import { Button } from 'react-native-elements'

import Animation from 'DB8/components/common/animations';
import * as alertAnimation from 'DB8/assets/animations/alert';

import textTransform from 'DB8/helpers/textTransform'

import { 
  scale, 
  verticalScale, 
  moderateScale 
} from 'react-native-size-matters';

const {width, height} = Dimensions.get('window')
export default class Alert extends Component {

  state={}
  onFirstOptionPress = () => {
    if(this.props.onFirstOptionPress){
      this.props.onFirstOptionPress()
    } 
  }

  onSecondOptionPress = () => {

    if(this.props.onSecondOptionPress){
      this.props.onSecondOptionPress()
    }
  }
  render() {

    return (
      <BlurView tint="dark" intensity={100} style={styles.container} >
        <View style={[{...styles.messageContainer, ...this.props.style}]} >
          <Text style={styles.messageTitle}>{textTransform(this.props.title, 'caps')}</Text>
          {this.props.title === 'Error' &&
            <AntDesign 
              style={{textAlign: 'center', width: '100%', paddingTop:10,paddingBottom: 20}} 
              color={Colors.red}  
              name="exclamationcircleo" 
              size={verticalScale(60)}  
            />
          }
          {this.props.title === 'Network Error' &&
            <Feather 
              style={{textAlign: 'center', width: '100%', paddingTop:10,paddingBottom: 20}} 
              color={Colors.red}  
              name="wifi-off" 
              size={verticalScale(60)}  
            />
          }
          <Text style={styles.messageInfo}>{this.props.text}</Text>

            {this.props.secondOption &&
              <View style={styles.btnGroup} >
                <Button onPress={this.onFirstOptionPress} buttonStyle={styles.doubleBtn}  type="clear" titleStyle={this.props.error ? [styles.btnText,{color: Colors.red}] : styles.btnText} title={this.props.firstOption} />
                <Button onPress={this.onSecondOptionPress} buttonStyle={styles.doubleBtn}  type="clear" titleStyle={this.props.error ? [styles.btnText,{color: Colors.red}] : styles.btnText} title={this.props.secondOption} />
              </View>
            }

            {!this.props.secondOption &&
              <View style={styles.btnGroup} >
                <Button onPress={this.onFirstOptionPress} buttonStyle={styles.btn}  type="clear" titleStyle={this.props.error ? [styles.btnText,{color: Colors.red}] : styles.btnText} title={this.props.firstOption} />
              </View>
            }

          
        </View>
      </BlurView>
    );
  }
}

Alert.defaultProps = {
  show: false,
  style: {},
  firstOption: "Okay",
  secondOption: null,
  error: false
}

const styles = StyleSheet.create({
  container:{
    position:'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,

  },
  alertAnimationContainer:{
    height: height/10,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15
  },
  messageContainer: {
    position: 'absolute',
    left: 10,
    right: 10,
    ...ifIphoneX({
      bottom: 30,
    },{
      bottom: 10
    }),
    padding: 15,
    height:'auto',

    alignItems: 'flex-start',
    backgroundColor: Colors.white,
    borderRadius:15,
    borderColor: Colors.grey,
    borderWidth: 1,
  },
  messageTitle:{
    fontSize: 22,
    fontFamily: 'roboto_bold',
    paddingBottom: 10,
    width: '100%',
    textAlign: 'center',
    textTransform: 'uppercase'
  },
  messageInfo:{
    fontSize: 17,
    textAlign: 'center',
    fontFamily: 'roboto',
    // flex:1,
    width: '100%',
  },
  btnGroup:{
    // justifyContent: 'flex-end',
    flexDirection: 'row',
    width: width - 55,
    paddingTop: verticalScale(15),
    // position: 'absolute',
  },
  doubleBtn:{
    padding: 0, 
    width: (width - 55)/2  
  },
  btn:{
    padding: 0,
    width:  (width - 55)
  },
  btnText:{
    color: Colors.defaultBlue,
    fontFamily: 'roboto_bold',
    fontSize: 17,
  }

});