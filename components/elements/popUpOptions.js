import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text} from 'react-native';
import { Overlay } from 'react-native-elements'
class PopUpOptions extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Overlay containerStyle={{alignItems: 'flex-end', flex: 1, justifyContent: 'left'}}overlayStyle={styles.container}>
              <Text>Hello from Overlay!</Text>
            </Overlay>        
        );
    }
}


const styles = StyleSheet.create({
  container: {
    height: 'auto',
    width: 'auto',

  }
});
export default PopUpOptions;
