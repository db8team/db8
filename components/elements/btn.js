import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet} from 'react-native';
import { Button } from 'react-native-elements'
class Btn extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        let {title, type, titleStyle, icon, containerStyle, buttonStyle, iconContainerStyle, gradientProps, onPress} = this.props
        buttonStyle = this.props.shape === 'circular' ? { ...buttonStyle,  height:this.props.rad, width: this.props.rad, borderRadius: this.props.rad/2} : {...buttonStyle}
        
        let btnProps = {
          title, 
          titleStyle, 
          icon, 
          containerStyle, 
          buttonStyle, 
          iconContainerStyle,
          onPress,
          type,
          

        }

        if (gradientProps) {
          btnProps = {
            ...btnProps, 
            linearGradientProps: gradientProps
          }

        }
        return (
            <Button 
              {...btnProps}
            />
        );
    }
}
Btn.defaultProps = {
  title : null, 
  titleStyle: {}, 
  icon: null, 
  containerStyle: {}, 
  buttonStyle:{}, 
  iconContainerStyle:{},
  type: 'solid',
  shape: 'rectangular',
  rad: 60,
  gradient: false,
  gradientProps: null,
}

// const styles = StyleSheet.create({
//   container: {
//     height: 50,
//     justifyContent: 'center',
//     paddingLeft: 10,
//     paddingRight: 10,
//   },
//   circularBtn: {

//   }
// });
export default Btn;
