import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { 
  StyleSheet,
  StatusBar,
  Platform
} from 'react-native';
import { Button } from 'react-native-elements'
import SafeAreaView from 'react-native-safe-area-view'
class Btn extends Component {
  render() {
    return (
      <SafeAreaView style={[styles.container, this.props.style]}>
        {this.props.children}
      </SafeAreaView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
  },

});
export default Btn;
