
import {
  Constants
} from 'expo';
const { manifest } = Constants;

const getConfig = () => {
  if (__DEV__) {
    return {
      backend: `http://${manifest.debuggerHost.split(":")[0]}:5000`
    }
  }else{
    return{
      backend: 'https://db8-backend.herokuapp.com'
    }
  }
}

const config = getConfig()
export default config