export default {
  defaultColor: '#b2b2b2',
  backgroundTransparent: 'transparent',
  // graident
  defaultBlue: '#0084ff',
  darkBlue: '#0072ff',
  lightBlue: '#00c6ff',
  blueGradient: ['#00c6ff','#0072ff'],
  leftBubbleBackground: '#f0f0f0',
  grey:'#f5f8fa',
  darkGrey:'#EBEBEB', 
  darkerGrey: '#B8B8B8',
  darkerGreyOpacity: 'rgba(0,0,0,0.7)',
  black: '#000',
  white: '#fff',
  transparentWhite: '#E5E5E5',
  red: '#FF0022',
  blackish: "#282828"
};
