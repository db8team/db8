import delay from 'DB8/helpers/delay'
import Config from 'DB8/config/config'
import {db} from 'DB8/utility/fire'

export const GET_DEBATES_REQUESTED = 'GET_DEBATES_REQUESTED'
export const GET_DEBATES_FAILED = 'GET_DEBATES_FAILED'
export const GET_DEBATES_SUCCESS = 'GET_DEBATES_SUCCESS'

export const GET_FINISHED_DEBATES_REQUESTED = 'GET_FINISHED_DEBATES_REQUESTED'
export const GET_FINISHED_DEBATES_FAILED = 'GET_FINISHED_DEBATES_FAILED'
export const GET_FINISHED_DEBATES_SUCCESS = 'GET_FINISHED_DEBATES_SUCCESS'

export const INSERT_USER_INTO_DEBATE_REQUESTED = 'INSERT_USER_INTO_DEBATE_REQUESTED'
export const INSERT_USER_INTO_DEBATE_FAILED = 'INSERT_USER_INTO_DEBATE_FAILED'
export const INSERT_USER_INTO_DEBATE_SUCCESS = 'INSERT_USER_INTO_DEBATE_SUCCESS'

export const UPDATE_DEBATES = 'UPDATE_DEBATES'
export const UPDATE_DEBATE = 'UPDATE_DEBATE'

export const PAIR_REQUESTED = 'PAIR_REQUESTED'
export const PAIR_FAILED = 'PAIR_FAILED'
export const PAIR_SUCCESS = 'PAIR_SUCCESS'
export const PAIR_WAITING = 'PAIR_WAITING'

export const CREATE_DEBATE_REQUESTED = 'CREATE_DEBATE_REQUESTED'
export const CREATE_DEBATE_FAILED = 'CREATE_DEBATE_FAILED'
export const CREATE_DEBATE_SUCCESS = 'CREATE_DEBATE_SUCCESS'

export const DELETE_DEBATE_REQUESTED = 'DELETE_DEBATE_REQUESTED'
export const DELETE_DEBATE_FAILED = 'DELETE_DEBATE_FAILED'
export const DELETE_DEBATE_SUCCESS = 'DELETE_DEBATE_SUCCESS'

export const END_DEBATE_REQUESTED = 'END_DEBATE_REQUESTED'
export const END_DEBATE_FAILED = 'END_DEBATE_FAILED'
export const END_DEBATE_SUCCESS = 'END_DEBATE_SUCCESS'

export const SEND_MESSAGES_REQUESTED = 'SEND_MESSAGES_REQUESTED'
export const SEND_MESSAGES_FAILED = 'SEND_MESSAGES_FAILED'
export const SEND_MESSAGES_SUCCESS = 'SEND_MESSAGES_SUCCESS'

export const LISTENER_DISPATCHED = 'LISTENER_DISPATCHED'
export const LISTENER_STOPED ='LISTENER_STOPED'
export const LISTENER_FAILED = 'LISTENER_FAILED'
export const SELECT_DEBATE = 'SELETEC_DEBATE'

export const updateDebates = (debates) => {
  return {
    type: UPDATE_DEBATES,
    payload: debates
  }
}

export const updateDebate = (debate) => {
  return {
    type: UPDATE_DEBATE,
    payload: debate
  }
}

// sendMessages actions
export const sendMessagesRequested = () => {
  return {
    type: SEND_MESSAGES_REQUESTED
  }
}

export const sendMessagesSuccess = () => {
  return {
    type: SEND_MESSAGES_SUCCESS
  }
}

export const sendMessagesFailed = (error) => {
  return {
    type: SEND_MESSAGES_FAILED,
    error
  }
}
export const sendMessages = (debate, newMessages) => {
  return async(dispatch,getState) => {
    const {fire,user} = getState()
    return db.runTransaction(function(transaction) {
      // This code may get re-run multiple times if there are conflicts.
      dispatch(sendMessagesRequested())
      let docRef = db.collection("debates").doc(debate.id);
      return transaction.get(docRef).then(function(doc) {
          if (!doc.exists) {
              throw "Document does not exist!";
          }
          let debate = {id: doc.id, ...doc.data()}
          debate.messages = [...newMessages,...debate.messages]
          transaction.update(docRef, { 
            messages:  [...debate.messages],
            status: "paired"
          });
          console.log(`transaction debate: ${debate.id}`)
          return debate

      });

  }).then(function(debate) {
      if (debate) {
        dispatch(sendMessagesSuccess())
      }
  }).catch(function(error) {
      console.log("Transaction failed: ", error);
      dispatch(sendMessagesFailed(error))
  });
  }
}

export const listenerDispatched = (unsubscribe) =>{
  return {
    type: LISTENER_DISPATCHED,
    payload: unsubscribe
  }
}

export const listnerStopped = (debate) =>{
  return {
    type: LISTENER_STOPED,
  }
}


export const stopListening = (debate,answer) => {
  return async(dispatch,getState) => {
    try{
      const {fire} = getState()
      if (debate.unsubscribe) {
        debate.unsubscribe()
      }
      dispatch(listnerStopped())
      dispatch(updateDebate({ unsubscribe: false}))
      let pairedDebate = await db.runTransaction(async (transaction) => {
          // This code may get re-run multiple times if there are conflicts.
          let docRef = db.collection("debates").doc(debate.id);
          let doc = await transaction.get(docRef)
          if (!doc.exists) {
              throw "Document does not exist!";
          }

          let pairedDebate = {id: doc.id, ...doc.data()}
          let newUsers = {...pairedDebate.users, 
            [answer] : [{
              ...pairedDebate.users[answer][0],
              listening: false
            }]
          }


          transaction.update(docRef, { 
            users:  newUsers
          });
          return {...pairedDebate, users: newUsers}
      })

      if (pairedDebate) {
        dispatch(updateDebate(pairedDebate))
      }
    }catch(error){
      console.log("Stop Listening Error: ", error);
    }
    
  }
}
export const initListener = (debate,answer, inital = true) => {
  return async(dispatch,getState) => {
    try{
      const {fire, questions} = getState()
      let oppAnswer = ''
      if (answer == 'answerA') {
        oppAnswer = 'answerB'
      }else{
        oppAnswer = 'answerA'
      }
      if (debate.unsubscribe) {
        debate.unsubscribe()
      }
      let unsubscribe = db.collection("debates").doc(debate.id)
        .onSnapshot(function(doc) {
          let updatedDebate = {id: doc.id, unsubscribe: unsubscribe , ...doc.data()}
          dispatch(updateDebate(updatedDebate))
          
      });
      dispatch(listenerDispatched(unsubscribe))
      let pairedDebate = await db.runTransaction(async (transaction) => {
          // This code may get re-run multiple times if there are conflicts.
          let docRef = db.collection("debates").doc(debate.id);
          let doc = await transaction.get(docRef)
          if (!doc.exists) {
              throw "Document does not exist!";
          }

          let pairedDebate = {id: doc.id, ...doc.data()}
          let newUsers = {...pairedDebate.users, 
            [answer] : [{
              ...pairedDebate.users[answer][0],
              listening: true
            }]
          }


          transaction.update(docRef, { 
            users:  newUsers
          });
          console.log(`transaction listening: ${pairedDebate.id}`)
          return pairedDebate
      })

      if (pairedDebate) {
        console.log("transaction successfull")
      }

      if (debate.type === 'live' && inital) {   
        
        let pairingClosingTime = questions.pairingTill
        let till = Math.ceil(pairingClosingTime.getTime() - new Date().getTime()) 
        await delay(till)
        console.log('tesssst')
        let updatedDoc = await getDebate(debate.id,fire)
        let updatedDebate = {id: debate.id, ...updatedDoc.data()}
        if (updatedDebate.users.answerA == "" || updatedDebate.users.answerB == "") {
          unsubscribe()
          await deleteDebate(updatedDebate,fire)
          console.log("Document successfully deleted!");
          dispatch(pairFailed())
        }
      }
    }catch(error){
      console.log("Transaction failed: ", error);
      dispatch({
          type: LISTENER_FAILED,
          error
        })
    }


  }
}

export const endDebateRequest = () => {
  return {
    type: END_DEBATE_REQUESTED
  }
}

export const endDebateSuccess = () => {
  return {
    type: END_DEBATE_SUCCESS
  }
}

export const endDebateFailed = (error) => {
  return {
    type: END_DEBATE_FAILED,
    error
  }
}

export const endDebate = (debate, initiated = true) => {
  return async(dispatch,getState) => {  
    try{
      console.log
      if (initiated) {
        dispatch(endDebateRequest())
        const {fire, nav} = getState()
        let updateDebate = db.collection("debates").doc(debate.id).update({
          ended: true
        })
        console.log('token.  ', nav)
        let uploadDebate = await fetch(`${Config.backend}/api/Debates`,{
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            token: nav.token
          },
          body:JSON.stringify({
            debate: {
              id: debate.id,
              messages: JSON.stringify(debate.messages),
              question: JSON.stringify(debate.question),
              question_id: debate.question.id,
              live: debate.type === "live",
              round: debate.round,
              crated: debate.created
            },
            users: [{
              uid: debate.users['answerA'][0].uid,
              answer: 'answerA'
            },{
              uid: debate.users['answerB'][0].uid,
              answer: 'answerB'
            }]
          })
        })
        dispatch(endDebateFailed())
        await updateDebate
        dispatch(endDebateSuccess())
        
      }else{
        dispatch(endDebateSuccess())
      }
    }catch(error){
      console.log(error)
      dispatch(endDebateFailed(error))
    } 
      
  }
}


//getFinishedDebates actions

const getFinishedDebatesRequested = () => {

  return {
    type: GET_FINSIHED_DEBATES_REQUESTED,
  }
}

const getFinishedDebatesSuccess = (debates = [],update= false) => {
  return {
    type: GET_FINSIHED_DEBATES_SUCCESS,
    debates,
    update
  }
}

const getFinishedDebatesFailed = (error) => {
  return {
    type: GET_FINSIHED_DEBATES_FAILED,
    error
  }
}
const getFinishedDebates = async (queryParams) => {
  return async (dispatch,getState) => {
    try{
      dispatch(getFinishedDebatesRequested())
      const {nav} = getState()
      let debates = await fetch(`${Config.backend}/api/Debates`,{
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          token: nav.token
        },
        body:JSON.stringify({
          where: queryParams
        })
      })
       console.log(debates)
    }catch(error){
      console.log('getDebatesError: ', error)
      dispatch(getFinishedDebatesFailed(error))
    }
  }
}



// getDebates actions
const getDebatesRequested = () => {

  return {
    type: GET_DEBATES_REQUESTED,
  }
}

const getDebatesSuccess = (debates = [],update= false) => {
  return {
    type: GET_DEBATES_SUCCESS,
    debates,
    update
  }
}

const getDebatesFailed = (error) => {
  return {
    type: GET_DEBATES_FAILED,
    error
  }
}
const getDebates = async (queryParams, fire, toPair = true) => {
  try{
    console.log('getDebates')

    let query = db.collection("debates")

    queryParams.forEach((param) => {
      if (param.type === 'orderBy'){
        if (param.order) {
          query = query.orderBy(param.value, param.order)
          // console.log('query.orderBy(param.value, param.order)')
        }else{
          query = query.orderBy(param.value)
          // console.log('query.orderBy(param.value)')
        }
      }else if (param.type ==='limit'){
        query = query.limit(param.value)
        // console.log('query.limit(param.value)')
      }else if (param.key) {
        query = query.where(param.key,param.comp,param.value)
        // console.log('query.where(param.key,param.comp,param.value)')
      }
    })
    let debatesSnapshot = await query.get()
    return debatesSnapshot
  }catch(error){
    console.log('getDebatesError: ', error)
    throw error
  }
}

const getDebate = async (docId,fire) => {
  let doc = await db.collection("debates").doc(docId).get()
  if (!doc.exists) {
    throw `Debate does not exist : ${docId}`
  }
  return doc
}

export const getDebatesRequest = (queryParams,filter, showLoading = true) => {
  return async (dispatch,getState) => {
    try{
      const {fire} = getState()
      if (showLoading) {
        dispatch(getDebatesRequested(showLoading))
      }
      
      let debatesSnapshot = await getDebates(queryParams,fire,false)
      let debates =[]
      debatesSnapshot.docs.forEach((doc) => {
        let d = {id: doc.id, ...doc.data()}
        if (filter) {
          let allow = filter(d)
          if (allow) {
            debates.push(d)
          }  
        }else{
          debates.push(d)
        }             
      })

      dispatch(getDebatesSuccess(debates,true))
    }catch(error){
      console.log('GET DEBATES FAILED: ',error)
      dispatch(getDebatesFailed(error))
    }

  }
}
// insertUserIntoDebate actions
const insertUserIntoDebateRequested = () => {
  return {
    type: INSERT_USER_INTO_DEBATE_REQUESTED
  }
}

const insertUserIntoDebateSuccess = () => {
  return {
    type: INSERT_USER_INTO_DEBATE_SUCCESS
  }
}

const insertUserIntoDebateFailed = () => {
  return {
    type: INSERT_USER_INTO_DEBATE_FAILED
  }
}

const insertUserIntoDebate = async (debate,user,answer,fire) => {
  try{
    let docRef = db.collection("debates").doc(debate.id);
    let pairedDebate = await db.runTransaction(async (transaction) => {
        // This code may get re-run multiple times if there are conflicts.
      let doc = await transaction.get(docRef)
      if (!doc.exists) {
          throw "Document does not exist!";
      }

      let pairedDebate = {id: doc.id, ...doc.data()}
      if (pairedDebate.users[answer] == "") {
        let newUsers = {...pairedDebate.users, 
          [answer] : [{uid: user.uid,pic_url:user.pic_url, display_name: user.display_name, listening: false}],
        }
        pairedDebate.users = newUsers
        pairedDebate.status = "paired"

        await transaction.update(docRef, { 
          users:  newUsers,
          usersArr: [...pairedDebate.usersArr, user.uid],
          status: "paired"
        });
        console.log(`transaction debate: ${pairedDebate.id}`)
        return pairedDebate
      }else{
        return null
      }
    })

    return pairedDebate

  }catch(error){
    console.log('insertUserIntoDebateError: ',error)
    throw error
  }
}

//create debate actions
const createDebateRequested = () => {
  return {
    type: CREATE_DEBATE_REQUESTED
  }
}

const createDebateSuccess = () => {
  return {
    type: CREATE_DEBATE_SUCCESS
  }
}

const createDebateFailed = (error) => {
  return {
    type: CREATE_DEBATE_FAILED,
    error
  }
}

const createDebate = async (debate,fire) => {
  let docRef = await db.collection("debates").add(debate)
  return docRef
}

//delete debate actions
const deleteDebateRequested = () => {
  return {
    type: DELETE_DEBATE_REQUESTED
  }
}

const deleteDebateSuccess = () => {
  return {
    type: DELETE_DEBATE_SUCCESS
  }
}

const deleteDebateFailed = (error) => {
  return {
    type: DELETE_DEBATE_FAILED,
    error
  }
}

const deleteDebate = async (debate,fire) => {
  try{
    await db.collection("debates").doc(debate.id).delete()
    return true
  }catch(error){
    console.log('deleteDebate Error: ',error)
    throw error
  }
}

// pair actions
const pairRequested = () => {
  return {
    type: PAIR_REQUESTED
  }
}

const pairSuccess = (debate,answer) => {
  console.log( `paired debate ${debate.id}: ${answer}`)
  return {
    type: PAIR_SUCCESS,
    debate,
    answer
  }
}

const pairWaiting = (debate,answer) => {
  return {
    type: PAIR_WAITING,
    debate,
    answer
  } 
}
const pairFailed = (error) => {
  return {
    type: PAIR_FAILED,
    error
  }
}
export const pairRequest = (question, answer,type = 'live',pairingClosingTime) => {
  return async (dispatch,getState) => {
    const {fire, user, questions} = getState()
    dispatch(pairRequested())

    console.log(question.id)
    let oppAnswer = ''
    if (answer == 'answerA') {
      oppAnswer = 'answerB'
    }else{
      oppAnswer = 'answerA'
    }
    try{
      if (type === 'live' && (Math.ceil(pairingClosingTime.getTime() - new Date().getTime()) <= 0)) {
        throw {message: 'Pairings are now closed.'}
      }
      // dispatch(getDebatesRequested(true))

      let debatesSnapshot = await getDebates(
        [
          {key: "type", comp: "==", value: type},
          {key: "status", comp: "==", value: "pairing"},
          {key: "question.id", comp: "==", value: question.id},
          {key: `users.${answer}`, comp: "==", value: ""},
          {type: "orderBy", value: "created"}
        ],
        fire
      )
      // dispatch(getDebatesSuccess())

      let paired = false

      for (var docIndex in debatesSnapshot.docs) {
        let doc = debatesSnapshot.docs[docIndex]
        let debate = {id: doc.id , ...doc.data()}
        if (debate.users[oppAnswer][0].uid != user.uid || true) {
          let pairedDebate = await insertUserIntoDebate(debate,user,answer,fire)
          if (pairedDebate) {
            paired = true
            dispatch(insertUserIntoDebateSuccess())
            dispatch(pairSuccess(pairedDebate,answer))
            break
          }else{
            dispatch(insertUserIntoDebateFailed())
          }
        }

      }
      if (!paired) {
        let users = {}
        if (answer == 'answerA') {
          users = {
            answerA: [{uid: user.uid, pic_url:user.pic_url,display_name: user.display_name, listening: false}],
            answerB: ""
          }
        }else{
          users = {
            answerB: [{uid: user.uid, pic_url:user.pic_url,display_name: user.display_name, listening:false}],
            answerA: ""
          }
        }
        let newDebate = {
          question,
          users: users,
          usersArr: [user.uid],
          status: "pairing",
          created: Date.now(),
          ended: false,
          round: questions.ms,
          type,
          messages: []
        }

        dispatch(createDebateRequested())
        let newDoc = await createDebate(newDebate,fire) 
        newDebate['id'] = newDoc.id
        dispatch(createDebateSuccess())
        dispatch(pairWaiting(newDebate,answer))

      }
    }catch(error){
      console.log(error)
      dispatch(pairFailed(error))
    }
  }
}

export const selectDebate = (debate, answer) => {
  return (dispatch) => {
    dispatch({
      type: SELECT_DEBATE,
      debate
    })
  }
}
export const adminDelete = () => {
  return async (dispatch,getState) => {
    const {fire} = getState()
    let debatesSnapshot = await getDebates(
        [
          {key: "created", comp: ">", value: 0},
        ],
        fire
      )
    await Promise.all(
      debatesSnapshot.docs.map(async (doc) => {
        let debate = {id: doc.id , ...doc.data()}
        await deleteDebate(debate,fire)
      })
    )

    console.log('done')
  }
}

