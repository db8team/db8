import { 
  BackHandler
} from 'react-native';

export const CHANGE_VIEW = 'CHANGE_VIEW'
export const CHANGE_NAV_COLOR = 'CHANGE_NAV_COLOR'
export const GO_BACK = 'GO_BACK'
export const ALERT = 'ALERT'
export const CLEAR_ALERT = 'CLEAR_ALERT'

export const changeView = (view, props= false) => {
  return{
    type: CHANGE_VIEW,
    view,
    props
  }  
}

export const changeNavColor = (dark = true) => {
  return{
    type: CHANGE_NAV_COLOR,
    navType: dark? 'dark' : 'light'
  }  
}
export const goBack = () => {
  return async (dispatch,getState) => {
    const {nav} = getState()
    console.log('test')
    if (nav.history.length) {
      dispatch({
        type: GO_BACK,
      })
    }else{
      BackHandler.exitApp()
      dispatch({})
    }
  }
  
}

export const alert = (message, alert = true) => {
  return {
      type: ALERT,
      error: {message}
    }
}

export const clearAlert = () => {
  return {
    type: CLEAR_ALERT
  }
}