import Config from 'DB8/config/config'
import { 
  AsyncStorage,
} from 'react-native';
import {firebase} from 'DB8/utility/fire'


export const SIGNIN_REQUESTED = 'SIGNIN_REQUESTED'
export const SIGNIN_FAILED = 'SIGNIN_FAILED'
export const SIGNUP_REQUESTED = 'SIGNUP_REQUESTED'
export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS'
export const SIGNUP_FAILED = 'SIGNUP_FAILED'
export const SIGNOUT_REQUESTED = 'SIGNOUT_REQUESTED'
export const SIGNOUT_SUCCESS = 'SIGNOUT_SUCCESS'
export const SIGNOUT_FAILED = 'SIGNOUT_FAILED'
export const SIGNIN_SUCCESS = 'SIGNIN_SUCCESS'
export const AUTH_INIT = 'AUTH_INIT'


export const signinSuccess = (user, jwtInit = true)  => {
  return (dispatch) => {
    dispatch({
      type: SIGNIN_SUCCESS,
      payload: user,
      jwtInit
    })
  }
}

export const loginWithFacebook = () => {
  return async(dispatch) => {
    dispatch({
      type: SIGNIN_REQUESTED
    })

    try {
      const result = await Expo.Facebook.logInWithReadPermissionsAsync(
        '514259105707439',
        { permissions: ['public_profile'] }
      );

      if (result.type === 'success') {
        // Build Firebase credential with the Facebook access token.
        const credential = firebase.auth.FacebookAuthProvider.credential(result.token);

        // Sign in with credential from the Facebook user.
        let authResult = await firebase.auth().signInAndRetrieveDataWithCredential(credential)
        // console.log(authResult)
      }else{
        throw result
      }
    } catch(error) {
      dispatch(signInFailed(error))
        
    }
  }
}
const firebaseAuth = (credential) => {

    // Sign in with credential from the Facebook user.
    firebase.auth().signInAndRetrieveDataWithCredential(credential).catch((error) => {
      // Handle Errors here.
      console.warn(error);
    });
}

export const loginWithGoogle = () => {
  return async(dispatch) => {
    dispatch({
      type: SIGNIN_REQUESTED
    })
    try {
      const result = await Expo.Google.logInAsync({

        androidClientId: "928464225835-jr9bk1jok01i9ar5vbnk21o8q5esk4rb.apps.googleusercontent.com",
        iosClientId: "928464225835-14qmstf21eavmdr34f2efvstohr4kkqe.apps.googleusercontent.com",
        scopes: ['profile', 'email'],
        behavior: "web"
      });

      if (result.type === 'success') {
        let authResult = await firebaseAuth(firebase.auth.GoogleAuthProvider.credential(result.idToken));
      } else {
        throw result
      }
    } catch(error) {
      dispatch(signInFailed(error))
    }
  }
}
export const signInFailed = (error, message = null) => {
  return {
    type: SIGNIN_FAILED,
    error : {error, message}
  }
} 
export const authInitListener = () => {

  return (dispatch, getState) => {
    dispatch({
      type: AUTH_INIT
    })
    return firebase.auth().onAuthStateChanged( async (user) => {
      try{
        console.log('auth status changed')
        if (user != null) {

          let response = await fetch(`${Config.backend}/api/Users/loginOrSignup`,{
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body:JSON.stringify({
              uid: user.uid,
              display_name: user.displayName,
              pic_url: user.photoURL,
              email: user.email,
              api_key: user.apiKey
            })
          })

          let result = await response.json()
          if (response.status != 200) {
            dispatch(signInFailed(result,"Sorry something went wrong !"))
          }else{

            dispatch(signinSuccess(result))
          }
          
        }else{
          let {user} = getState()
          await AsyncStorage.removeItem('jwtToken');
          dispatch({
            type: SIGNOUT_SUCCESS
          })
        } 
      }catch(error){
        dispatch(signInFailed(error,"Sorry something went wrong !"))
      }
    }); 
  }
  
}
export const signOut = () => async (dispatch, getState) => {
  let{fire} = getState()
  dispatch({
    type: SIGNOUT_REQUESTED
  })
  await firebase.auth().signOut()
  await AsyncStorage.removeItem("jwtToken");
  dispatch({
    type: SIGNOUT_SUCCESS
  })
  
}