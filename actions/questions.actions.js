import {db} from 'DB8/utility/fire'

export const GET_QUESTIONS_REQUESTED = 'GET_QUESTIONS_REQUESTED'
export const GET_QUESTIONS_FAILED = 'GET_QUESTIONS_FAILED'
export const GET_QUESTIONS_SUCCESS = 'GET_QUESTIONS_SUCCESS'

export const GET_TIME_TILL_LIVE_REQUESTED = 'GET_TIME_TILL_LIVE_REQUESTED'
export const GET_TIME_TILL_LIVE_FAILED = 'GET_TIME_TILL_LIVE_FAILED'
export const GET_TIME_TILL_LIVE_SUCCESS = 'GET_TIME_TILL_LIVE_SUCCESS'

import Config from 'DB8/config/config'

export const getQuestionsRequest = () => {
  return {
    type: GET_QUESTIONS_REQUESTED
  }
}

export const getQuestionsSuccess = (questions) => {
  return {
    type: GET_QUESTIONS_SUCCESS,
    live: questions.live,
    practice: questions.practice,
    all: questions.all
  }
}

export const getQuestionsFailed = (error) => {
  return {
    type: GET_QUESTIONS_FAILED,
    error,
  }
}

export const getQuestions = () => {
  return (dispatch,getState) => {
    const {fire} = getState()
    dispatch(getQuestionsRequest())

    let query = db.collection("questions").where(`active`,'==',true)

    return query
    .get()
    .then(function(querySnapshot) {
        let questions = {live: {all:[]}, practice: [], all: []}

        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            let question = {
              ...doc.data(),
              id: doc.id
            }
            questions.all.push(question)
            if (question.live) {
              question.categories.forEach((category) =>{
                if (category in questions.live) {
                  questions.live[category].push(question)
                }else{
                  questions.live[category] = [question]
                }
                questions.live.all.push(question)
              })
            }else{
              questions.practice.push(question)
            }
        });
        dispatch(getQuestionsSuccess(questions))
    })
    .catch(function(error) {

        console.log("Error getting documents: ", error);
        dispatch(getQuestionsFailed(error))
    });
    
  }
}

export const getTimeTillLiveRequest = () => {
  return {
    type: GET_TIME_TILL_LIVE_REQUESTED
  }
}

export const getTimeTillLiveSuccess = (liveTime,pairingTill,ms) => {
  return {
    type: GET_TIME_TILL_LIVE_SUCCESS,
    liveTime,
    pairingTill,
    ms
  }
}

export const getTimeTillLiveFailed = (error) => {
  return {
    type: GET_TIME_TILL_LIVE_FAILED,
    error,
  }
}


export const getTime =() => {
  return (dispatch,getState) => {
    let timeBeforeRequest = new Date()
    let timeDiff = null

    dispatch(getTimeTillLiveRequest())
    return fetch(`${Config.backend}/api/Debates/getTimeTillLive`,{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then(response => {
      return response.json()
    })
    .then(function(result) {
      let timeAtResponse = new Date().getTime()
      let timeToExecute = timeAtResponse - timeBeforeRequest.getTime()
      let deltaTime = result.deltaTime
      let liveTime = new Date(timeAtResponse + deltaTime)
      let pairingTill = new Date((timeAtResponse + deltaTime) + (result.pairingLength * 60000))
      // console.log({liveTime,pairingTill,deltaTime,time:timeAtResponse + deltaTime})
      dispatch(getTimeTillLiveSuccess(liveTime,pairingTill,result.ms))
      
    })
    .catch(error => {
      dispatch(getTimeTillLiveFailed(error))
    })
  }

}