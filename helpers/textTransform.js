const textTransform = (text, transform = 'caps') => {
  if (isNaN(text)) {
    if (transform === 'caps') {
      return text.toUpperCase()
    } else if (transform === 'upperCase') {
      return text.split(' ').map(
        (s) => s.charAt(0).toUpperCase() + s.substring(1)
      ).join(' ');
    } 
  }
  
  return text
}

export default textTransform