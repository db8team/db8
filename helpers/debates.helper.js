export const addDetailsToDebate = (debate, uid) => {

  if (!debate) {
    return null
  }
  if (debate.users.answerA === ""){
    return {
      ...debate,
      opponent: null,
      opAnswer: 'answerA',
      answer: 'answerB',
      live: debate.type === 'live' ? true : false
    }

  }else if (debate.users.answerB === ""){
    return {
      ...debate,
      opponent: null,
      opAnswer: 'answerB',
      answer: 'answerA',
      live: debate.type === 'live' ? true : false
    }
  }else{
    let answerAUser = debate.users.answerA[0]
    let answerBUser = debate.users.answerB[0]
    let opponent = {...answerBUser}
    if (answerBUser.uid === uid) {
      opponent  = {...answerAUser}
      return {
        ...debate,
        opponent,
        opAnswer : 'answerA',
        answer : 'answerB',
        live: debate.type === 'live' ? true : false
      }

    }else{
     return {
      ...debate,
      opponent,
      opAnswer : 'answerB',
      answer : 'answerA',
      live: debate.type === 'live' ? true : false
     } 
    }
  }
}

